#include <iostream>
#include <climits>
#include <cmath>
#include <vector>

using namespace std;

vector< vector<int> > Stacks;

/*
int take(int a, int b) {
    if (a < 0 || b < 0) {
        return 0; 
    }
    if (Memo[a][b] < 0) {
        if (Stacks[0][a] == Stacks[1][b]) {
            Memo[a][b] =  1 + take(a - 1, b - 1);    
        } else {
            Memo[a][b] = max(take(a - 1, b), take(a, b - 1));
        }
         
    }
    return Memo[a][b];
}
*/
int take(int idx[], int n) {
    bool b = false;
    for (int i = 0; i < n; i++) {
        b |= idx[i] >= 0;
    }
    if (!b) { return 0; }

    int best = 0;
    for (int i = 0; i < n; i++) {
        if (idx[i] >= 0) {
            int tmp[5]; 
            for (int k = 0; k < n; k++) {
                tmp[k] = idx[k];
            }
            int count = 1;
            tmp[i]--;
            for (int j = i + 1; j < n; j++) {
                if (idx[j] >= 0 && Stacks[i][idx[i]] == Stacks[j][idx[j]]) {
                    count++; 
                    tmp[j]--;
                }
            }
            int q = count > 1 ? pow(2, count - 2) : 0;
            best = max(best, q + take(tmp , n));
        }
    }
    return best;
}


int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N; cin >> N;
        int Size[5];
        for (int n = 0; n < N; n++) {
            cin >> Size[n];
        }
        Stacks = vector< vector<int> > (N);
        for (int n = 0; n < N; n++) {
            for (int s = 0; s < Size[n]; s++) {
                int color; cin >> color;
                Stacks[n].push_back(color);
            }
        }
        int idx[5];
        for (int s = 0; s < 5; s++) {
            idx[s] = Size[s] - 1;
        }
        
        // cout << take(Size[0] - 1, Size[1] - 1) << "\n";
        cout << take(idx, N) << "\n";
    }
}
