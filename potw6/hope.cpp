#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, no_property> Graph;


int main() {
    ios_base::sync_with_stdio(false);
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int K, S, M; cin >> K >> S >> M;
        Graph G(K * S);
        for (int m = 0; m < M; m++) {
            int u, v, H; cin >> u >> v >> H;
            for (int h = 0; h < H; h++) {
                int x, y; cin >> x >> y;
                add_edge(u * S + x, v * S + y, G);
            }
        }

        vector<int> matemap(K * S);
        edmonds_maximum_cardinality_matching(G, &matemap[0]);

        cout << K * S - matching_size(G, &matemap[0]) << endl;
    }
    return 0;
}
