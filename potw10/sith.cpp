#include <iostream>
#include <vector>
#include <cmath>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/connected_components.hpp>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;
using namespace boost;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef Triangulation::Edge_iterator Edges;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, no_property> Graph;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;


int main() {
    ios_base::sync_with_stdio(false);
    int T; cin >> T;
    for (int q = 0; q < T; q++) {
        int N, R; cin >> N >> R;
        
        vector<K::Point_2> Planets(N);
        map<K::Point_2, Vertex> Map;
        for (int n = 0; n < N; n++) {
            cin >> Planets[n];
            Map.insert(pair<K::Point_2, Vertex>(Planets[n], n));
        }


        // do binary search over N
        int val = 0;
        int left = 0, right = N; // (N + 1)/ 2;
        while(left < right) {
            int K = (right + left + 1) / 2;

            Graph G;
            Triangulation t; t.insert(Planets.begin() + K, Planets.end());
            for (Edges e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
                if (t.segment(e).squared_length() <= pow(R, 2)) {
                    K::Point_2 u = t.segment(e).source();
                    K::Point_2 v = t.segment(e).target();
                    add_edge(Map[u], Map[v], G);
                }
            }
            if (num_vertices(G)) {
                // feasible
                vector<int> C(num_vertices(G));
                int ncc = connected_components(G, &C[0]);
                vector<int> CS(ncc);
                for (int i = 0; i < C.size(); i++) {
                    CS[C[i]]++;
                }
                if (*max_element(CS.begin(), CS.end()) >= K) {
                    val = K;
                    left = K;        
                    continue;
                } 
            }
            // unfeasible
            right = K - 1;
        }
        val ? cout << val << endl : cout << 1 << endl;
    }
    return 0;
}
