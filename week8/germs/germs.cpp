#include <iostream>
#include <vector>
#include <cmath>
#include <climits>
#include <algorithm>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

// Namespaces
using namespace std;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> T;
typedef T::Edge_iterator Edges;
typedef T::All_vertices_iterator Vertices;
typedef T::Vertex_handle Vertex;

typedef T::All_faces_iterator Faces;
typedef T::Face_handle Face;

double sucks(const K::FT x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a == -0 ? -a : a;
}

// Main function looping over the testcases
int main() {
    ios_base::sync_with_stdio(false);
    int N, M;
    cin >> N;
    while(N) {
        int left, bottom, right, top;
        cin >> left >> bottom >> right >> top;
        vector<K::Point_2> Germs(N);

        map<K::Point_2, K::FT> D;
        for (int n = 0; n < N; n++) {
            int x, y; cin >> x >> y;
            K::Point_2 p(x, y);
            K::FT l = CGAL::squared_distance(p, K::Point_2(left, y)) * 4;
            K::FT b = CGAL::squared_distance(p, K::Point_2(x, bottom)) * 4;
            K::FT r = CGAL::squared_distance(p, K::Point_2(right, y)) * 4;
            K::FT t = CGAL::squared_distance(p, K::Point_2(x, top)) * 4;
            D[p] = min(l, min(b, min(r, t)));
            Germs[n] = p;
        }

        T t; t.insert(Germs.begin(), Germs.end());
        for (Edges e = t.finite_edges_begin(); e != t.finite_edges_end(); ++e) {
            K::FT d = t.segment(e).squared_length();
            K::Point_2 source = t.segment(e).source();
            K::Point_2 target = t.segment(e).target();
            D[source] = min(D[source], d);
            D[target] = min(D[target], d);
        }

        K::FT minD = DBL_MAX;
        K::FT maxD = 0;
        vector<K::FT> dist(N);
        int i = 0; 
        map<K::Point_2, K::FT>::iterator it = D.begin();
        while (it != D.end()) {
            dist[i] = it->second;
            i++; it++;
        }
        sort(dist.begin(), dist.end());
        int first = 0;
        while(dist[0] > 4 * pow(first, 4) + 4 * pow(first, 2) + 1) {
            first++;
        }
        cout << first;
        int mean = first;
        while(dist[N / 2] > 4 * pow(mean, 4) + 4 * pow(mean, 2) + 1) {
            mean++;
        }
        cout << " " << mean;
        int last = mean;
        while(dist[N - 1] > 4 * pow(last, 4) + 4 * pow(last, 2) + 1) {
            last++;
        }
        cout << " " << last << endl;
        cin >> N;
    }
    return 0;
}
