#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

// Namespaces
using namespace std;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> T;
typedef T::Vertex_handle Vertex;
typedef T::All_faces_iterator Faces;
typedef T::Face_handle Face;

double sucks(const K::FT x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a == -0 ? -a : a;
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int N, M;
    cin >> N;
    while(N) {
        vector<K::Point_2> I(N);
        for (int n = 0; n < N; n++) {
            cin >> I[n];
        }
        T t; t.insert(I.begin(), I.end());
        cin >> M;
        for (int m = 0; m < M; m++) {
            K::Point_2 p;
            double d;
            cin >> p >> d;
            if (d == 0) {
                cout << "y";
                continue;
            }
            Face origin = t.locate(p);
            if (d > squared_distance(p, t.nearest_vertex(p, origin)->point())) {
                cout << "n";
                continue;
            }
            char c = 'n';
            set<Face> visited;
            stack<Face> dfs; dfs.push(origin);
            while (!dfs.empty()) {
                Face item = dfs.top(); dfs.pop();
                visited.insert(item);
                if (t.is_infinite(item)) {
                    c = 'y';
                    break;
                }
                for (int i = 0; i < 3; i++) { 
                    Face f = item->neighbor(i);
                    Vertex v = item->vertex((i + 1) % 3);
                    Vertex u = item->vertex((i + 2) % 3);
                    if (visited.find(f) == visited.end() && d * 4 <= squared_distance(v->point(), u->point())) {
                        dfs.push(f);
                    }
                }
            }
            cout << c;
        }
        cout << "\n";
    
        cin >> N;
    }
	return 0;
}
