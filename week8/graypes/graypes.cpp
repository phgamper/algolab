#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>



// Namespaces
using namespace std;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> T;
typedef T::Edge_iterator Edges;

double sucks(const K::FT x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a == -0 ? -a : a;
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int N;
    cin >> N;
    while(N) {
        vector<K::Point_2> Apes(N);
        for (int n = 0; n < N; n++) {
            cin >> Apes[n];
        }

        T t; t.insert(Apes.begin(), Apes.end());
        K::FT min = DBL_MAX;
        for (Edges e = t.finite_edges_begin(); e != t.finite_edges_end(); e++) {
            K::FT d = squared_distance(t.segment(e).source(), t.segment(e).target());
            min = d < min ? d : min;
        }

        cout << sucks(50 * sqrt(min)) << "\n";
    
        cin >> N;
    }
	return 0;
}
