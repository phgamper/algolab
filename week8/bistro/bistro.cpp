#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

// Namespaces
using namespace std;


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> T;
typedef T::Vertex_handle Vertex;

double sucks(const K::FT x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a == -0 ? -a : a;
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int N, M;
    cin >> N;
    while(N) {
        vector<K::Point_2> R(N);
        for (int n = 0; n < N; n++) {
            cin >> R[n];
        }

        T t; t.insert(R.begin(), R.end());

        cin >> M;
        for (int m = 0; m < M; m++) {
            K::Point_2 p;
            cin >> p;
            Vertex v = t.nearest_vertex(p);
            cout  << setiosflags(ios::fixed) << setprecision(0) << sucks(squared_distance(v->point(), p)) << "\n";
        }
    
        cin >> N;
    }
	return 0;
}
