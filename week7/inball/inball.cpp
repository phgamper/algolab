#include <iostream>
#include <stdexcept>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz ET;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


double sucks(const CGAL::Quotient<ET> x)
{
    double a = floor(CGAL::to_double(x));
    if(a != DBL_MAX){
    while (a > x){ a--; }
    while (a+1 <= x) { a++; }
    }
    return a == -0 ? 0 : a;
}


int main()
{
    ios_base::sync_with_stdio (false);
    int N;
    cin >> N;
    while(N) {
        int D;
        cin >> D;

        Program lp (CGAL::SMALLER, false, 0, false, 0);
        lp.set_l(D, true, 0);
        for (int n = 0; n < N; n++) {
            double norm = 0;
            for (int d = 0; d < D; d++) {
                int a;
                cin >> a;
                lp.set_a(d, n, a);
                norm += a * a;
            }
            int b;
            cin >> b;
            lp.set_b(n, b);
            // TODO why?
            lp.set_a(D, n, sqrt(norm));
        }

        // TODO why?
        lp.set_c(D, -1);

        Solution s = solve_linear_program(lp, ET());

        if (s.status() == CGAL::QP_INFEASIBLE)
            cout << "none" << endl;
        else if (s.status() == CGAL::QP_UNBOUNDED)
            cout << "inf" << endl;
        else {
            cout << setiosflags(ios::fixed) << setprecision(0) << sucks(-s.objective_value()) << endl;
        }

        cin >> N;
    };

    return 0;
}
