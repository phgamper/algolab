#include <iostream>
#include <stdexcept>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz ET;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


double sucks(const CGAL::Quotient<ET> x)
{
    double a = floor(CGAL::to_double(x));
    while (a > x){ a--; }
    while (a+1 <= x) { a++; }
    return a;
}


int main()
{
    ios_base::sync_with_stdio (false);
    int N, M;
    cin >> N >> M;
    while(N || M) {

        Program lp (CGAL::SMALLER, true, 0, false, 0);
        // right hand side, i.e. bounds
        for (int n = 0; n < N; n++) {
            int l, u;
            cin >> l >> u;
            lp.set_b(2 * n, -l);
            lp.set_b(2 * n + 1, u);
        }



        for (int m = 0; m < M; m++) {
            int c;
            cin >> c;
            // f(x, y, ..) = c0x + c1y + ...
            lp.set_c(m ,c); 
            for (int n = 0; n < N; n++) {
                int val;
                cin >> val;
                lp.set_a(m, 2 * n, -val);
                lp.set_a(m, 2 * n + 1, val);
            }
        }

        Solution s = solve_nonnegative_quadratic_program(lp, ET());

        if (s.status() == CGAL::QP_INFEASIBLE)
            cout << "No such diet." << endl;
        else if (s.status() == CGAL::QP_UNBOUNDED)
            cout << "unbounded" << endl;
        else {
            cout << setiosflags(ios::fixed) << setprecision(0) << sucks(s.objective_value()) << endl;
        }

        cin >> N >> M;
    };

    return 0;
}
