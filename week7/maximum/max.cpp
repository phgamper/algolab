#include <iostream>
#include <stdexcept>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz ET;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


double sucks(const CGAL::Quotient<ET> x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a;
}

void maximize(int a, int b) {
    Program qp (CGAL::SMALLER, true, 0, false, 0);
    const int X = 0;
    const int Y = 1;
    qp.set_a(X, 0, 1); qp.set_a(Y, 0, 1); qp.set_b(0, 4);
    qp.set_a(X, 1, 4); qp.set_a(Y, 1, 2); qp.set_b(1, a * b);
    qp.set_a(X, 2, -1); qp.set_a(Y, 2, 1); qp.set_b(2, 1);

    qp.set_d(X, X, 2*a); qp.set_c(Y, -b); // minimize ax² - by

    Solution s = solve_nonnegative_quadratic_program(qp, ET());

    if (s.status() == CGAL::QP_INFEASIBLE)
        cout << "no" << endl;
    else if (s.status() == CGAL::QP_UNBOUNDED)
        cout << "unbounded" << endl;
    else {
        cout << setiosflags(ios::fixed) << setprecision(0) << -sucks(s.objective_value()) << endl;
    }

}

void minimize(int a, int b) {
    Program qp (CGAL::SMALLER, true, 0, false, 0);
    const int X = 0;
    const int Y = 1;
    const int Z = 2;
    qp.set_a(X, 0, 1); qp.set_a(Y, 0, 1); qp.set_b(0, 4);
    qp.set_a(X, 1, 4); qp.set_a(Y, 1, 2); qp.set_a(Z, 1, -1); qp.set_b(1, a * b);
    qp.set_a(X, 2, -1); qp.set_a(Y, 2, 1); qp.set_b(2, 1);
    
    qp.set_d(X, X, 2*a); qp.set_c(Y, -b); qp.set_d(Z, Z, 2);

    Solution s = solve_nonnegative_quadratic_program(qp, ET());

    if (s.status() == CGAL::QP_INFEASIBLE)
        cout << "no" << endl;
    else if (s.status() == CGAL::QP_UNBOUNDED)
        cout << "unbounded" << endl;
    else {
        cout << setiosflags(ios::fixed) << setprecision(0) << sucks(s.objective_value()) << endl;
    }

}

int main()
{
    ios_base::sync_with_stdio (false);
    int p;
    cin >> p;
    while(p) {
        int a, b;
        cin >> a >> b;

        p == 1 ? maximize(a, b) : minimize(a, b);

        cin >> p;
    };

    return 0;
}
