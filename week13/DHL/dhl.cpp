#include<iostream>
#include<vector>
#include<climits>

using namespace std;

int N;
vector< vector<int> > A;
vector< vector<int> > B;

vector< vector<int> > Take;

/*
int take(int a, int b) {
    // take all
    int cost = A[a][N - 1] * B[b][N - 1];
    if (a < N - 1 && b < N - 1) {
        // take a subset
        for (int i = a; i < N - 1; i++) {
            for (int k = b; k < N - 1; k++) {
                cost = min(A[a][i] * B[b][k] + take(i + 1, k + 1), cost);
            }
        }
    }
    return cost;
}
*/

int memo(int a, int b) {
    if (Take[a][b] < 0) {
        // take all
        int cost = A[a][N - 1] * B[b][N - 1];
        if (a < N - 1 && b < N - 1) {
            // take a subset
            for (int i = a; i < N - 1; i++) {
                for (int k = b; k < N - 1; k++) {
                    cost = min(A[a][i] * B[b][k] + memo(i + 1, k + 1), cost);
                }
            }
        }
        Take[a][b] = cost;
    }
    return Take[a][b];
}

int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        cin >> N;
        Take = vector< vector<int> >(N, vector<int>(N, -1));
        A = vector< vector<int> >(N, vector<int>(N));
        for (int n = 0; n < N; n++) {
            int a; cin >> a;
            A[n][n] = a - 1;
            for (int k = 0; k < n; k++) {
                A[k][n] = A[k][n - 1] + a - 1;
            }
        }
        B = vector< vector<int> >(N, vector<int>(N));
        for (int n = 0; n < N; n++) {
            int b; cin >> b;
            B[n][n] = b - 1;
            for (int k = 0; k < n; k++) {
                B[k][n] = B[k][n - 1] + b - 1;
            }
        }

        cout << memo(0, 0) << "\n";
    }
    return 0;
}
