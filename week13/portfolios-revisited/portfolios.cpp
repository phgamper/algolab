#include <iostream>
#include <stdexcept>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>

using namespace std;

typedef CGAL::Gmpz ET;

typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

double sucks(const CGAL::Quotient<ET> x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x){ a++; }
    while (a-1 >= x) { a--; }
    return a;
}

int main()
{
    ios_base::sync_with_stdio (false);
    int N, M;
    cin >> N >> M;
    while(N || M) {
        vector<int> Cost(N), Return(N);
        for (int n = 0; n < N; n++) {
            cin >> Cost[n] >> Return[n];
        }

        vector< vector<int> > Varianz(N, vector<int>(N));

        for (int n = 0; n < N; n++) {
            for (int k = 0; k < N; k++) {
                cin >> Varianz[n][k];
            }
        }

        Program lp (CGAL::LARGER, true, 0, false, 0);
        for (int n = 0; n < N; n++) {
            lp.set_a(n, 0, Cost[n]);
            lp.set_a(n, 1, Return[n]);

            for (int k = 0; k <= n; k++) {
                lp.set_d(n, k, 2 * Varianz[n][k]);
            }
        }
        lp.set_r(0, CGAL::SMALLER);
        lp.set_r(1, CGAL::LARGER);

        for (int m = 0; m < M; m++) {
            int C, V;
            cin >> C >> V;
            lp.set_b(0, C);

            // find upper bound
            int R = 32;
            lp.set_b(1, R);
            Solution s = solve_nonnegative_quadratic_program(lp, ET());
            while(s.status() != CGAL::QP_INFEASIBLE) {
                R = R * 2;
                lp.set_b(1, R);
                s = solve_nonnegative_quadratic_program(lp, ET());
            }

            // do binary search
            int cost = 0;
            int left = 0, right = R;
            while(left < right) {
                R = (left + right + 1) / 2;
                lp.set_b(1, R);
                s = solve_nonnegative_quadratic_program(lp, ET());
                if (s.status() ==  CGAL::QP_OPTIMAL) {
                    if (CGAL::to_double(s.objective_value()) > V) {
                        right = R - 1; 
                    } else {
                        left = R;
                        cost = max(cost, R);
                    }
                } else {
                    right = R - 1; 
                }
            }
            cout << cost << "\n";
        }

        cin >> N >> M;
    };

    return 0;
}
