#include <iostream>
#include <cassert>
#include <vector>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

using namespace std;
using namespace CGAL;

// choose exact floating-point type
#ifdef CGAL_USE_GMP
#include <CGAL/Gmpz.h>
typedef CGAL::Gmpz ET; 
#else
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
#endif

// program and solution types
typedef CGAL::Quadratic_program<int> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

int main(){
    ios_base::sync_with_stdio(false);
    int N,M;
    cin >> N >> M;
    
    while(N != 0 && M != 0){
    
        Program qp (CGAL::LARGER, true, 0, false, 0); 
        //read in assets
        for(int i = 0; i < N; i++){
                int cost, r;
                cin >> cost >> r;
                qp.set_a(i,0,cost);
                qp.set_a(i,1,r);
        }

        //read covariances
        for(int i = 0; i < N; i++){
            for(int j = 0; j <N; j++){
                int cov;
                cin >> cov;
                if(j < i+1){
                    qp.set_d(i,j,2*cov);
                    cout << i << j << cov << endl;
                }
            }
        }
        
        qp.set_r(0,CGAL::SMALLER);
        qp.set_r(1,CGAL::LARGER);
        //read in friends
        for(int j = 0; j < M; j++){
            int totalCost, maxVar;
            cin >> totalCost >> maxVar;
            
            qp.set_b(0,totalCost);
            int maxReturn = 32;
            int lower = 0;
        
            qp.set_b(1,maxReturn);
            cout << maxReturn << endl;
            Solution s = CGAL::solve_quadratic_program(qp, ET());
            while(s.status() != CGAL::QP_INFEASIBLE){
                maxReturn = maxReturn * 2;
                qp.set_b(1,maxReturn);
                cout << maxReturn << endl;
        
                // solve the program, using ET as the exact type
                s = CGAL::solve_quadratic_program(qp, ET());
            }
            
            //do binary search
            int R = maxReturn;
            int L = 0;
            int bestReturn = 0;
            while(L < R){
                maxReturn = (R+L+1)/2;
                cout << L << R << maxReturn << endl;
                qp.set_b(1,maxReturn);
                // solve the program, using ET as the exact type
                Solution s = CGAL::solve_quadratic_program(qp, ET());
                assert (s.solves_quadratic_program(qp));

                // output
                if (s.status() == CGAL::QP_OPTIMAL) {
                    if(CGAL::to_double(s.objective_value()) > maxVar){
                        //decrease
                        R= maxReturn-1;
                    }else{
                        //increase
                        L = maxReturn;
                        bestReturn = std::max(bestReturn,maxReturn);
                    }
                }else{
                    R = maxReturn-1;
                }
            }

            // cout << bestReturn << "\n";
        }
        
        cin >> N >> M;
    }
    
    return 0;
}
