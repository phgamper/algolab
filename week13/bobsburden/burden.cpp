#include <iostream>
#include <vector>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace std;
using namespace boost;


typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;

typedef property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResidualCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef graph_traits<Graph>::edge_descriptor Edge;

void addEdge(int u, int v, long c, long w, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap, Graph &G) {
    cout << u << " -> " << v << ": " << c << "/" << w << "\n";
    Edge e, reverseE;
    tie(e, tuples::ignore) = add_edge(u, v, G);
    tie(reverseE, tuples::ignore) = add_edge(v, u, G);
    capacitymap[e] = c;
    weightmap[e] = w;
    capacitymap[reverseE] = 0;
    weightmap[reverseE] = -w;
    revedgemap[e] = reverseE; 
    revedgemap[reverseE] = e; 
}

int main() {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {
        int K; cin >> K;
        int nodes = K * (K + 1) / 2;
        int src = nodes;
        int sink = nodes + 1;
        // cout << "src: " << src << ", sink: " << sink << "\n";
        Graph G(nodes + 2);
        EdgeCapacityMap cmap = get(edge_capacity, G);
        EdgeWeightMap wmap = get(edge_weight, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);

        for (int k = 0; k < K; k++) {
            for (int i = 0; i <= k; i++) {
                long v; cin >> v;
                int u = k * (k + 1) / 2 + i;
                // cout << u << " / " << v << " - ";
                if (i > 0) { // not left border
                    addEdge(u, u - 1, 2, v, cmap, wmap, rmap, G);
                    // cout << u - 1 << " ";
                    if (k > 0) { // not top line
                        addEdge(u, u - k - 1, 2, v, cmap, wmap, rmap, G);
                        // cout << u - k - 1 << " ";
                    }
                }
                if (i < k) { // not right border
                    addEdge(u, u + 1, 2, v, cmap, wmap, rmap, G);
                    // cout << u + 1 << " ";
                    if (k > 0) { // not top line
                        addEdge(u, u - k, 2, v, cmap, wmap, rmap, G);
                        // cout << u - k << " ";
                    }
                }
                if (k < K - 1) { // not bottom line
                    addEdge(u, u + k + 1, 2, v, cmap, wmap, rmap, G);
                    // cout << u + k + 1 << " ";
                    addEdge(u, u + k + 2, 2, v, cmap, wmap, rmap, G);
                    // cout << u + k + 2 << " ";
                }
                // cout << "\n";
            }
        }
        addEdge(src, 0, 2, 0, cmap, wmap, rmap, G);
        // two sinks with capacity 1 each
        addEdge(nodes - 1, sink, 1, 0, cmap, wmap, rmap, G);
        addEdge(nodes - K, sink, 1, 0, cmap, wmap, rmap, G);
        // cout << nodes - 1 << " " << nodes - K << "\n";

        // int flow = push_relabel_max_flow(G, src, sink);
        // cycle_canceling(G);

        successive_shortest_path_nonnegative_weights(G, src, sink);
        cout << find_flow_cost(G) << "\n";
    }
        
    return 0;
}
