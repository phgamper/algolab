#include <iostream>
#include <vector>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace std;
using namespace boost;


typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_weight_t, int> > Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_weight_t >::type EdgeWeightMap;

int main() {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {
        int K; cin >> K;
        int nodes = K * (K + 1) / 2;
        Graph G(nodes);
        vector<int> weights(nodes);
        EdgeWeightMap W = get(edge_weight, G);

        for (int k = 0; k < K; k++) {
            for (int i = 0; i <= k; i++) {
                Edge e;	bool b;
                long v; cin >> v;
                int u = k * (k + 1) / 2 + i;
                weights[u] = v;
                if (i > 0) { // not left border
                    tie(e, b) = add_edge(u, u - 1, G);
                    W[e] = v;
                    if (k > 0) { // not top line
                        tie(e, b) = add_edge(u, u - k - 1, G);
                        W[e] = v;
                    }
                }
                if (i < k) { // not right border
                    tie(e, b) = add_edge(u, u + 1, G);
                    W[e] = v;
                    if (k > 0) { // not top line
                        tie(e, b) = add_edge(u, u - k, G);
                        W[e] = v;
                    }
                }
                if (k < K - 1) { // not bottom line
                    tie(e, b) = add_edge(u, u + k + 1, G);
                    W[e] = v;
                    tie(e, b) = add_edge(u, u + k + 2, G);
                    W[e] = v;
                }
            }
        }

        vector< vector<Vertex> > predmap(3, vector<Vertex>(nodes));
        vector< vector<int> > distmap(3, vector<int>(nodes));
        int start[3] = {0, nodes - K, nodes - 1};
        for (int i = 0; i < 3; i++) {
            dijkstra_shortest_paths(G, start[i], predecessor_map(make_iterator_property_map(predmap[i].begin(), get(vertex_index, G))).distance_map(make_iterator_property_map(distmap[i].begin(), get(vertex_index, G))));
        }

        int cost = INT_MAX;
        for (int i = 0; i < nodes; i++) {

            cost = min(weights[i] + distmap[0][i] + distmap[1][i] + distmap[2][i], cost);
        }

        cout << cost << "\n";
    }
        
    return 0;
}
