#include <iostream>
#include <vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/connected_components.hpp>


using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;

typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;


void addEdge(int from, int to, long capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap, Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M, S; cin >> N >> M >> S;

        Graph G(N + 2);
        EdgeCapacityMap cmap = get(edge_capacity, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);
        int source = N;
        int sink = N + 1;

        vector<int> degree(N, 0);
        // sweepers
        for (int s = 0; s < S; s++) {
            int val; cin >> val; 
            addEdge(source, val, 1, cmap, rmap, G);
            degree[val]++;
        }
        // doors
        for (int s = 0; s < S; s++) {
            int val; cin >> val; 
            addEdge(val, sink, 1, cmap, rmap, G);
            degree[val]++;
        }
        for (int m = 0; m < M; m++) {
            int u, v; cin >> u >> v;
            addEdge(u, v, 1, cmap, rmap, G);
            addEdge(v, u, 1, cmap, rmap, G);
            degree[u]++;
            degree[v]++;
        }
        // is eulerian
        bool b = true;
        for (int n = 0; n < N; n++) {
            b &= (bool) !(degree[n] % 2);
        }
        vector<int> C(N + 2);
        // int ncc = connected_components(G, make_iterator_property_map(C.begin(), get(vertex_index, G))); 
        int ncc = connected_components(G, &C[0]);
        b &= ncc == 1;
        long flow = push_relabel_max_flow(G, source, sink);
        b &= flow == S;
        b ? cout << "yes\n" : cout << "no\n";
    }
    return 0;
}
