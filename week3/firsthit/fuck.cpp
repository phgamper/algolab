#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>
#include <climits>
#include <cmath>
#include <CGAL/intersections.h>
#include <CGAL/utils.h>
#include <iomanip>

using namespace std;
typedef CGAL::Exact_predicates_exact_constructions_kernel EK;
typedef EK::Segment_2 S;
typedef EK::Ray_2 R;
typedef EK::Point_2 P;

int main()
{
    ios_base::sync_with_stdio (false);
    int n;
    cin >> n;
    while(n){
        long x, y, a, b;
        cin >> x >> y >> a >> b;
        R ray = R(P(x, y),P(a, b));

        bool flag = false;

        P source = P(x, y);
        EK::FT dist = DBL_MAX;
        P hit;

        for(unsigned int i = 0; i < n; i++){
            double s1,s2,e1,e2;
            cin >> s1 >> e1 >> s2 >> e2;

            auto res = intersection(ray, S(P(s1,e1), P(s2,e2)));

            //do not break after first hit, we need closest hit to be outputted
            if(res){
                flag = true;
                if (const P* op = boost::get<P>(&*res)){
                    EK::FT tmp  = squared_distance(source, *op);
                    if(dist > tmp){
                        hit = *op;
                        dist = tmp;
                    }
                }else if (const S* os = boost::get<S>(&*res)){
                    EK::FT mind1 = squared_distance(source, os->source());
                    EK::FT mind2 = squared_distance(source, os->target());
                    if(dist > mind1 && mind1 <= mind2){
                        hit = os->source();
                        dist = mind1;
                    }else if(dist > mind2 && mind2 < mind1){
                        hit = os->target();
                        dist = mind2;
                    }
                }
            }
        }

        //decide if the ray hits obstacle
        if(!flag){
            cout << "no" << "\n";
        }else{
            cout << setprecision(0) << fixed << CGAL::to_double(hit.x()) << " " << CGAL::to_double(hit.y()) << "\n";
        }   

        cin >> n;
    }
    return 0;
}
