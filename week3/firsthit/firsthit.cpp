#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/intersections.h>
#include <iostream>
#include <stdexcept>
#include <climits>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Ray_2 R;
typedef K::Segment_2 S;


double sucks(const K::FT x)
{
    double a = floor(CGAL::to_double(x));
    if(a != DBL_MAX){
        while (a > x){ a -= 1; }
        while (a+1 <= x) { a += 1; }
    }
    return a;
}


int main()
{
    ios_base::sync_with_stdio (false);
    int n;
    cin >> n;
    while(n) {
        double a, b, x, y;
        cin >> x >> y >> a >> b;
        P source = P(x,y);
        R ray = R(source, P(a,b));
        bool hit = false;
        K::FT d = DBL_MAX; 
        P h;
        for (int i = 0; i < n; i++) {
            double r, s, t, u;
            cin >> r >> s >> t >> u;
            S seg = S(P(r,s), P(t,u));
            if(CGAL::do_intersect(ray, seg)) {
                auto res = CGAL::intersection(ray, seg);
                if(res) {
                    if (const P *q = boost::get<P>(&*res)) {
                        K::FT tmp = CGAL::squared_distance(source, *q);
                        if(d > tmp){
                            d = tmp;
                            h = *q;
                        }
                    } else if (const S *q = boost::get<S>(&*res)) {
                        K::FT k = CGAL::squared_distance(source, q->source());
                        K::FT l = CGAL::squared_distance(source, q->target());
                        K::FT tmp = k < l ? k : l;
                        if(d > tmp){
                            d = tmp;
                            h = k < l ? q->source() : q->target();
                        }
                    }
                    hit = true;
                }
            }
        }
        if(hit) {
            cout << setprecision(0) << fixed << sucks(h.x()) << " " << sucks(h.y()) << "\n";
        } else {
            cout << "no\n";
        }

        cin >> n;
    };

    return 0;
}
