#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Ray_2 R;
typedef K::Segment_2 S;
typedef K::Triangle_2 T;

int main()
{
    ios_base::sync_with_stdio (false);
    int c;
    cin >> c;
    for(int i = 0; i < c; i ++) {
        int m, n;
        cin >> m >> n;
        vector<P> legs = vector<P>(m);
        for(int j = 0; j < m; j++) {
            int x, y;
            cin >> x >> y;
            legs[j] = P(x, y);
        }

        vector<T> maps = vector<T>(n);
        for(int k = 0; k < n; k++) {
            int q0, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11; 
            cin >> q0 >> q1 >> q2 >> q3 >> q4 >> q5 >> q6 >> q7 >> q8 >> q9 >> q10 >> q11; 
            S t0 = S(P(q0, q1), P(q2, q3));
            S t1 = S(P(q4, q5), P(q6, q7));
            S t2 = S(P(q8, q9), P(q10, q11));

            P a, b, c;
            auto res = CGAL::intersection(t0, t1);
            if(res) {
                if (const P *q = boost::get<P>(&*res)) {
                    a = *q;
                }
            }
            res = CGAL::intersection(t1, t2);
            if(res) {
                if (const P *q = boost::get<P>(&*res)) {
                    b = *q;
                }
            }
            res = CGAL::intersection(t2, t0);
            if(res) {
                if (const P *q = boost::get<P>(&*res)) {
                    c = *q;
                }
            }

            maps[k] = T(a, b, c);
        }

        int l = 0, u = n - 1;
        bool flag = true;
        while(flag) {
            for(int j = l; j < u; j++) {

            }

            l = flag ? l + 1 : l;
        }

    }

    return 0;
}
