#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <stdexcept>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Ray_2 R;
typedef K::Segment_2 S;

int main()
{
    ios_base::sync_with_stdio (false);
    int n;
    cin >> n;
    while(n) {
        bool inter = false;
        long a, b, x, y;
        cin >> x >> y >> a >> b;
        R ray = R(P(x,y), P(a,b));
        long r, s, t, u;
        int i = 0;
        while (i < n) {
            i++;
            cin >> r >> s >> t >> u;
            S seg = S(P(r,s), P(t,u));
            // auto res = CGAL::do_intersect(ray, seg);
            auto res = CGAL::intersection(ray, seg);
            if(res) {
                inter = true;
                break;
            }
        }

        while(i < n) {
            i++;
            cin >> r >> s >> t >> u;
        }

        if(inter) {
            cout << "yes\n";
        } else {
            cout << "no\n";
        }

        cin >> n;
    };

    return 0;
}
