#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <CGAL/Min_circle_2.h>
#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt K;
typedef CGAL::Min_circle_2_traits_2<K> MCT;
typedef CGAL::Min_circle_2<MCT> MC;
typedef K::Point_2 P;
typedef K::Ray_2 R;
typedef K::Segment_2 S;


double sucks(const K::FT x)
{
    double a = ceil(CGAL::to_double(x));
    while (a < x) a += 1;
    while (a-1 >= x) a -= 1;
    return a;
}


int main()
{
    ios_base::sync_with_stdio (false);
    int n;
    cin >> n;
    while(n) {
        vector<P> citizens = vector<P>(n);
        for(int i = 0; i < n; i++) {
            double x, y;
            cin >> x >> y;
            citizens[i] = P(x, y);
        }

        MC mc(citizens.begin(), citizens.end(), true);
        MCT::Circle circle = mc.circle();
        K::FT d = CGAL::sqrt(circle.squared_radius());
        cout << setprecision(0) << fixed << sucks(d) << "\n";
        cin >> n;
    };

    return 0;
}
