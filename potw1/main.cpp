#include <iostream>
#include <vector>
#include <climits>

using namespace std;

int main(void) {
    int T, n, i, j, k;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n >> k; 
        vector<int> deck(n+1, 0);
        for(int q = 0; q < n; q++){
            cin >> deck[q];
        }

        int x = 0, y = 0, sum = deck[y], best = INT_MAX;
        while(x < n && y < n){
            /*
            cout << " - v: " << deck[y];
            cout << " - sum: " << sum;
            cout << " - best: " << best;
            cout << " - x/y " << x << "/" << y;
            cout << " - i/j " << i << "/" << j << "\n";
            */
            int tmp = abs(k - sum);
            /* break if found exact match
               if(tmp == 0){
               i = x;
               j = y; 
               break;
               } else 
               */
            if(tmp < best) {
                best = tmp;
                i = x;
                j = y;
            }    
            if(sum >= k) {
                sum -= deck[x]; 
                if(x == y) {
                    y++;
                    sum += deck[y];
                }
                x++;
            } else {
                y++;
                sum += deck[y];
            }
                /*
            } else {
                sum -= deck[x]; 
                if(x == y) {
                    y++;
                    sum += deck[y];
                }
                x++;
            }
            */
        }
        while(x < n) {
            int tmp = abs(k - sum);
            if(tmp < best) {
                best = tmp;
                i = x;
                // set y to last slot
                j = n - 1;
            }
            sum -= deck[x];
            x++;
        }
        cout << i << " " << j << "\n";
    }
}
