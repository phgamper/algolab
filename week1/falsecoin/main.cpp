#include <iostream>
#include <vector>

using namespace std;

const char EQ = '=';
const char LT = '<';
const char GT = '>';
const char NA = 'u';


void merge(vector<char> *coins, vector<char> *tmp){
   for(int i = 0; i < coins->size(); i++) {
        char old = coins->at(i);
        char cur = tmp->at(i);
        if(cur == EQ) {
            coins->at(i) = EQ;
        } else if (old == NA) {
            coins->at(i) = cur;
        } else if (old != cur) {
            coins->at(i) = EQ;
        }
   }
}    

int main(void) {
    char sign;
    int T, n, k, P;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n >> k; 
        vector<char> coins(n, NA);
        for(int i = 0; i < k; i++){
            cin >> P;
            vector<int> left(P); 
            vector<int> right(P); 
            for(int p = 0; p < P; p++){
                cin >> left[p];
            }
            for(int p = 0; p < P; p++){
                cin >> right[p];
            }
            cin >> sign;
            if (sign == EQ) {
                for(int p = 0; p < P; p++){
                    coins[left[p]-1] = EQ; 
                    coins[right[p]-1] = EQ; 
                }
            } else if (sign == GT) {
                vector<char> tmp(n, EQ);
                for(int p = 0; p < P; p++){
                    tmp[left[p]-1] = GT;
                    tmp[right[p]-1] = LT;
                }
                merge(&coins, &tmp);
            } else if (sign == LT) {
                vector<char> tmp(n, EQ);
                for(int p = 0; p < P; p++){
                    tmp[left[p]-1] = LT;
                    tmp[right[p]-1] = GT;
                }
                merge(&coins, &tmp);
            } 
        }

        int coin = 0;
        int na = 0;
        int na_idx = 0;
        int i = 0;
        while(!coin && i < n){
            if(coins[i] == NA) {
                na++;
                na_idx = i + 1;
            }else if(coins[i] == LT || coins[i] == GT){
                // found it
                coin = i + 1;
            } 
            i++;
        }
        while(i < n){
            if(coins[i] == LT || coins[i] == GT){
                // found another candidate
                coin = 0;
                na = 0;
                na_idx = 0;
                break;
            } 
            i++;
        }
        coin = !coin && na == 1 ? na_idx : coin;
        cout << coin << "\n";
    }
}
