#include <iostream>

using namespace std;

int main(void) {
    int N, n;
    double value;
    cin >> N;
    for(int i = 0; i < N; i++){
        double sum = 0;
        cin >> n;
        for(int k = 0; k < n; k++){
            cin >> value;
            sum += value;
        }
        cout << sum << "\n";
    }
}
