#include <iostream>
#include <vector>

using namespace std;

/**
 * Basic idea, count all even E_i and odd O_i pairs, if i + 1 is even
 * increase the counter by E_i + 1, otherwise by O_i
 */
int main(void) {
    int N, n, value = 0;
    cin >> N;
    for(int k = 0; k < N; k ++){
        int ctr = 0;
        cin >> n;
        vector<int> S(n+1,0);
        vector<int> E(n+1,0);
        vector<int> O(n+1,0);
        for(int i = 1; i <= n; i++){
            cin >> value;
            S[i] = value + S[i-1];
            if(S[i] % 2 == 0) {
                E[i] = E[i-1] + 1;
                O[i] = O[i-1];
            } else {
                E[i] = E[i-1];
                O[i] = O[i-1] + 1;
            }
        }
        for(int j = 1; j <= n; j++){
            if(S[j] % 2 == 0){
                ctr += E[j-1] + 1;
            } else {
                ctr += O[j-1];
            }
        }
        cout << ctr << "\n";
    }
    return 0;
}
