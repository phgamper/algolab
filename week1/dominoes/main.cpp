#include <iostream>
#include <vector>

using namespace std;

int main(void) {
    std::ios_base::sync_with_stdio(false);
    int N, n, h;
    cin >> N;
    for(int k = 0; k < N; k++){
        cin >> n;
        int pos = 1;
        int i = 1;
        while(i <= pos && i <= n){
            cin >> h;
            int tmp = i + h - 1;
            pos = max(tmp, pos);
            i++;
        }
        while(i <= n) {
            cin >> h;
            i++;
        }
        pos = min(pos, n);
        cout << pos << "\n";
    }
    return 0;
}
