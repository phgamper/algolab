#include <iostream>
#include <vector>

using namespace std;


int evenpairs(int n, vector<int> *seq){
    int ctr = 0;
    vector<int> S(n+1,0);
    vector<int> E(n+1,0);
    vector<int> O(n+1,0);
    for(int i = 1; i <= n; i++){
        S[i] = seq->at(i-1) + S[i-1];
        if(S[i] % 2 == 0) {
            E[i] = E[i-1] + 1;
            O[i] = O[i-1];
        } else {
            E[i] = E[i-1];
            O[i] = O[i-1] + 1;
        }
    }
    for(int j = 1; j <= n; j++){
        if(S[j] % 2 == 0){
            ctr += E[j-1] + 1;
        } else {
            ctr += O[j-1];
        }
    }
    return ctr;
}

int main(void) {
    int N, n, value;
    cin >> N;
    for(int k = 0; k < N; k ++){
        int ctr = 0;
        cin >> n;
        vector< vector<int> > M(n, vector<int>(n+1,0));
        vector< vector< vector<int> > > Q(n, vector< vector<int> >(n, vector<int>(n,0)));
        for(int i = 0; i < n; i++){
            for(int j = 1; j <= n; j++){
                cin >> value;
                M[i][j] = (value + M[i][j-1]) % 2; 
            }
        }
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                for(int k = j; k < n; k++){
                    Q[j][k][i] = (M[i][k+1] - M[i][j]) % 2;
                }
            }
        }
         for(int j = 0; j < n; j++){
            for(int k = j; k < n; k++){
                ctr += evenpairs(n, &Q[j][k]); 
            }
        }
        cout << ctr << "\n";
    }
    return 0;
}
