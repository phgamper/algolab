#include<iostream>
#include<vector>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef Triangulation::Edge_iterator Edges;
typedef Triangulation::Vertex_handle VHandle;

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, property<edge_weight_t, K::FT> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_weight_t>::type WeightMap;


int main() {

    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M;
        K::FT P;
        cin >> N >> M >> P;

        vector<K::Point_2> J(N);
        for (int n = 0; n < N; n++) {
            cin >> J[n];
        }
        Graph G;
        Graph maxG;
        WeightMap wmap = get(edge_weight, G);
        WeightMap maxW = get(edge_weight, maxG);
        int idx = 0, maxI = 0;
        K::FT maxP = DBL_MIN;
        K::FT minP = DBL_MIN;
        map<VHandle, int> Map;
        map<VHandle, int> maxM;
        Triangulation dt; dt.insert(J.begin(), J.end());
        for (Edges e = dt.finite_edges_begin(); e != dt.finite_edges_end(); ++e) {
            VHandle u = e->first->vertex((e->second + 1) % 3);
            VHandle v = e->first->vertex((e->second + 2) % 3);
            K::FT d = dt.segment(e).squared_length();
            if (d <= P) {
                int a, b;
                if (Map.find(u) == Map.end()) {
                    a = ++idx;
                    Map.insert(pair<VHandle, int>(u, a));
                } else {
                    a = Map[u];
                }
                if (Map.find(v) == Map.end()) {
                    b = ++idx;
                    Map.insert(pair<VHandle, int>(v, b));
                } else {
                    b = Map[v];
                }
                add_edge(a, b, d, G);
            }
            int a, b;
            if (maxM.find(u) == maxM.end()) {
                a = ++maxI;
                maxM.insert(pair<VHandle, int>(u, a));
            } else {
                a = maxM[u];
            }
            if (maxM.find(v) == maxM.end()) {
                b = ++maxI;
                maxM.insert(pair<VHandle, int>(v, b));
            } else {
                b = maxM[v];
            }
            add_edge(a, b, d, maxG);
        }

        vector<int> C(idx);
        connected_components(G, &C[0]);

        for (int m = 0; m < M; m++) {
            K::Point_2 a, b;
            cin >> a >> b;
            VHandle u = dt.nearest_vertex(a);
            VHandle v = dt.nearest_vertex(b);
            K::FT ad = squared_distance(a, u->point()) * 4;
            K::FT bd = squared_distance(b, v->point()) * 4;
            maxP = max(ad, maxP);
            maxP = max(bd, maxP);
            cout << maxP << " - fuck \n";
            if (ad <= P && bd <= P) {
                if (Map.find(u) != Map.end() && Map.find(v) != Map.end() && C[Map[u]] == C[Map[v]]) {
                    minP = max(ad, minP);
                    minP = max(bd, minP);
                    cout << "y";
                    continue;
                }
            }
            cout << "n";
        }
        cout << "\n";

        vector<Edge> mst;
        kruskal_minimum_spanning_tree(maxG, back_inserter(mst));
        for (vector<Edge>::iterator e = mst.begin(); e != mst.end(); e++) {
            maxP = max(maxW[*e], maxP);
            cout << maxP << " - it \n";
        }

        cout << setiosflags(ios::fixed) << setprecision(0) << maxP << "\n";
        
        mst.clear();
        kruskal_minimum_spanning_tree(G, back_inserter(mst));
        for (vector<Edge>::iterator e = mst.begin(); e != mst.end(); e++) {
            minP = max(wmap[*e], minP);
        }

        cout << setiosflags(ios::fixed) << setprecision(0) << minP << "\n";
    }
    return 0;
}
