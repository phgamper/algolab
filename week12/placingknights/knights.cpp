#include<iostream>
#include<vector>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, no_property> Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N; cin >> N;
        //Graph G(N * N);
        Graph G(N * N);
        vector< vector<bool> > Board(vector<vector<bool> >(N, vector<bool>(N)));
        int holes = 0;
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                bool b; cin >> b;
                Board[i][j] = b;
                holes += Board[i][j] ? 0 : 1;
            }
        }

        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                int v, u = j * N + i;
                if (Board[i][j]) {
                    if (j + 2 < N && i - 1 >= 0 && Board[i - 1][j + 2]) {
                        v = (j + 2) * N + (i - 1);
                        add_edge(u, v, G); 
                    }
                    if (j + 1 < N && i - 2 >= 0 && Board[i - 2][j + 1]) {
                        v = (j + 1) * N + (i - 2);
                        add_edge(u, v, G); 
                    }
                    if (j + 1 < N && i + 2 < N && Board[i + 2][j + 1]) {
                        v = (j + 1) * N + (i + 2);
                        add_edge(u, v, G); 
                    }
                    if (j + 2 < N && i + 1 < N && Board[i + 1][j + 2]) {
                        v = (j + 2) * N + (i + 1);
                        add_edge(u, v, G); 
                    }
                }
            }
        }
        vector<Vertex> mate(N * N);
        edmonds_maximum_cardinality_matching(G, &mate[0]);
        cout << N * N - matching_size(G, &mate[0]) - holes << "\n";

    }
}
