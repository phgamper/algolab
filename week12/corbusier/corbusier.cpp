#include<iostream>
#include<vector>

using namespace std;

int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, I, K; cin >> N >> I >> K;
        vector<int> H(N);
        for (int n = 0; n < N; n++) {
            int h; cin >> h;
            H[n] = h % K;
            // cout << H[n] << " ";
        }
        // cout << "\n";

        vector<vector<bool> > DP(N, vector<bool>(K));
        for (int k = 0; k < K; k++) {
            DP[0][k] = (H[0] % K == k);
        }

        for (int n = 1; n < N; n++) {
            for (int k = 0; k < K; k++) {
                // int idx = k - H[n] < 0 ? K - (k - H[n]) : k - H[n];
                int idx = k - H[n] % K;
                idx = idx < 0 ? K + idx : idx;
                DP[n][k] = DP[n-1][k] || DP[n-1][idx] || k == H[n];
            }
        }

        /*
        for (int k = 0; k < K; k++) {
            cout << "k: " << k << " - ";
            for (int n = 0; n < N; n++) {
                cout << DP[n][k] << " "; 
            }
            cout << "\n";
        }
        */

        // cout << I << " ... ";
        for (int n = 0; n < N; n++) {
            // cout << DP[n][I] << " ";
            if (DP[n][I]) {
                cout << "yes\n"; 
                break;
            }
            
            if (n == N - 1) {
                cout << "no\n"; 
            }
        }
    }
    
    return 0;
}
