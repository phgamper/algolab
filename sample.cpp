#include <iostream>
#include <queue>

using namespace std;

// bool lt (int i, int j) { return (i > j); }


int main (void){
    int n, val;
    cin >> n;
    priority_queue<int, vector<int>, greater<int> > q;
    for(int i = 0; i < n; i++){
        cin >> val; 
        q.push(val);
    }
    while(!q.empty()){
        cout << q.top() << " ";
        q.pop();
    }

    cout << "\n";

    return 0;
}
