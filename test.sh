#!/bin/bash

TMP_BIN="/tmp/test.out"
TMP_OUT="out.bin"

if [ -z "$1" ]; then
	echo "too few arguments"
	exit 2
fi

if [ -z "$2" ]; then
	echo "too few arguments"
	exit 2
fi

g++ -Wall -O3 -o $TMP_BIN $1 

chmod +x $TMP_BIN

$TMP_BIN < "$2.in" > $TMP_OUT
diff "$2.out" $TMP_OUT

usage() {
    echo "Usage: $0 SOURCE_CODE TEST_SET"
    echo "" 
}

