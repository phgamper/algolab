#include <iostream>
#include <vector>
#include <climits>
#include <cmath>


using namespace std;

int N;
int Side;
vector<int> Planks;
vector< vector<int> >Sets;

int foo(int i) {
    if (i <  N) {
        int count = 0;
        for (int j = 0; j < 4; j++) {
            Sets[j].push_back(i);
            count += foo(i + 1);
            Sets[j].pop_back();
        }
        return count ? 1 : 0;
    } else {
        for (int j = 0; j < 4; j++) {
            int sum = 0;
            for (int k = 0; k < Sets[j].size(); k++) {
                sum += Planks[Sets[j][k]];
            }
            if (sum != Side) {
                return 0;
            }
        }
        return 1;
    }
}

int main() {

    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        cin >> N;
        Planks = vector<int>(N);
        Sets = vector< vector<int> > (4);
        int sum = 0;
        for (int n = 0; n < N; n++) {
            cin >> Planks[n];
            sum += Planks[n];
        }

        if (sum % 4 != 0) {
            cout << 0 << "\n";
        } else {
            Side = sum / 4;
            cout << foo(0) << "\n";
        }
    }

    return 0;
}
