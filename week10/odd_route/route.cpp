#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, directedS, no_property, property<edge_weight_t, int> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph, edge_weight_t>::type WeightMap;


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M, source, target;
        cin >> N >> M;
        Graph G(4 * N);
        cin >> source >> target;
        WeightMap W = get(edge_weight, G);
        for (int m = 0; m < M; m++) {
            int u, v, w;
            cin >> u >> v >> w;
            Edge e;
            if (w % 2) { // Odd weight
                tie(e, tuples::ignore) = add_edge(u, v + 3 * N, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + N, v + 2 * N, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + 2 * N, v + N, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + 3 * N, v, G); W[e] = w;
            } else {
                tie(e, tuples::ignore) = add_edge(u, v + N, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + N, v, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + 2 * N, v + 3 * N, G); W[e] = w;
                tie(e, tuples::ignore) = add_edge(u + 3 * N, v + 2 * N, G); W[e] = w;
            }
        }

        // Find distance from node 0 to the node furthest from it.
        vector<int> dmap(4 * N);
        dijkstra_shortest_paths(G, source, distance_map(&dmap[0]));
        if (dmap[target + 3 * N] != INT_MAX ) {
            cout << dmap[target + 3 * N] << "\n"; 
        } else {
            cout << "no" << "\n";
        }
    }
	return 0;
}
