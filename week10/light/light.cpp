#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <iostream>
#include <stdexcept>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef Triangulation::Vertex_handle Vertex;

int main()
{
    ios_base::sync_with_stdio (false);
    int T;
    cin >> T;
    for (int q = 0; q < T; q++) {
        int M, N, H;
        cin >> M >> N;
        vector<int> R(M);
        vector<K::Point_2> P(M);
        for (int m = 0; m < M; m++) {
            cin >> P[m] >> R[m];
        }

        cin >> H;

        vector<K::Point_2> L(N);
        for (int n = 0; n < N; n++) {
            cin >> L[n];
        }

        // do binary search on the minimum number of lights
        int l = 0, r = N;
        while (l < r) {
            bool alive = false;
            int i = (r + l + 1) / 2;
            Triangulation t; t.insert(L.begin(), L.begin() + i);
            for (int m = 0; m < M; m++) {
                Vertex v = t.nearest_vertex(P[m]);
                K::FT D = CGAL::squared_distance(v->point(), P[m]);
                K::FT d = R[m] + H;
                if(D >= d * d) {
                    alive = true;
                    break;
                }
            }

            if (alive) {
                l = i;
            } else {
                r = i - 1;
            }
        }

        // print the result
        Triangulation t; t.insert(L.begin(), L.begin() + l);
        for (int m = 0; m < M; m++) {
            Vertex v = t.nearest_vertex(P[m]);
            K::FT D = CGAL::squared_distance(v->point(), P[m]);
            K::FT d = R[m] + H;
            if(D >= d * d) {
                cout << m << " ";
            }
        }
        cout << "\n";
    }

    return 0;
}
