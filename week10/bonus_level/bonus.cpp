#include <iostream>
#include <vector>
#include <climits>

using namespace std;

/*
   int N; 
   vector<vector<int> > Board;

   int bottomup(int i, int j){
   if (i < 0 || j < 0) {
   return 0;
   }
   return Board[i][j] + max(bottomup(i - 1, j), bottomup(i, j - 1));
   }

   int topdown(int i, int j){
   if (i >= N || j >= N) {
   return 0;
   }
   return Board[i][j] + max(topdown(i + 1, j), topdown(i, j + 1));
   }
   */

int main() {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {
        int N; cin >> N;
        vector<vector<int> > Board(N, vector<int>(N));
        vector<vector<int> > DP(N + 1, vector<int>(N + 1, 0));

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cin >> Board[i][j];
                DP[i+1][j+1] = max(DP[i][j+1], DP[i+1][j]) + Board[i][j];
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cout << Board[i][j] << " ";
            }
            cout << "\n";
        }
            cout << "\n";

        for (int i = 0; i <= N; i++) {
            for (int j = 0; j <= N; j++) {
                cout << DP[i][j] << " ";
            }
            cout << "\n";
        }
            cout << "\n";

        int coins = DP[N][N];

        int i = N; int j = N;
        while(i && j) {
            int prev = DP[i][j] - Board[i-1][j-1];
            Board[i-1][j-1] = 0;
            if (prev == DP[i][j - 1]) {
                j--;
            } else {
                i--;
            }

        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                DP[i+1][j+1] = max(DP[i][j+1], DP[i+1][j]) + Board[i][j];
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cout << Board[i][j] << " ";
            }
            cout << "\n";
        }
            cout << "\n";

        for (int i = 0; i <= N; i++) {
            for (int j = 0; j <= N; j++) {
                cout << DP[i][j] << " ";
            }
            cout << "\n";
        }
            cout << "\n";

        cout << coins + DP[N][N] << "\n";
    }
    return 0;
}



