#include <iostream>
#include <vector>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;
typedef property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResidualCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef graph_traits<Graph>::edge_descriptor Edge;

void addEdge(int u, int v, long c, long w, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap, Graph &G) {
    Edge e, reverseE;
    tie(e, tuples::ignore) = add_edge(u, v, G);
    tie(reverseE, tuples::ignore) = add_edge(v, u, G);
    capacitymap[e] = c;
    weightmap[e] = w;
    capacitymap[reverseE] = 0;
    weightmap[reverseE] = -w;
    revedgemap[e] = reverseE; 
    revedgemap[reverseE] = e; 
}

int main() {

    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N; cin >> N;
        Graph G(2 * pow(N, 2));
        int src = 0;
        int sink = pow(N, 2) - 1;
        long cost = 0;
        EdgeCapacityMap cmap = get(edge_capacity, G);
        EdgeWeightMap wmap = get(edge_weight, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                int val;
                cin >> val;
                if (i == 0 && j == 0) {
                    cost += (100 - val);
                    addEdge(0, pow(N, 2), 2, 0, cmap, wmap, rmap, G);  
                } else {
                    if (i == N - 1 && j == N - 1) {
                        cost -= (100 - val);
                    }
                    int left = (j - 1) + i * N;
                    int top = j + (i - 1) * N;
                    int cur = j + i * N;
                    if (i > 0 && 0 <= top && top < pow(N, 2)) {
                        addEdge(top + pow(N, 2), cur, 1, 100 - val, cmap, wmap, rmap, G);  
                    }

                    if (j > 0 && 0 <= left && left < pow(N, 2)) {
                        addEdge(left + pow(N, 2), cur, 1, 100 - val, cmap, wmap, rmap, G);  
                    }
                    addEdge(cur, cur + pow(N, 2), 1, 0, cmap, wmap, rmap, G);  
                }
            }
        }


        successive_shortest_path_nonnegative_weights(G, src, sink);
        cost += find_flow_cost(G);
        cout << 2 * (2 * N - 2) * 100 - cost << "\n";
    }
}
