#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, property<edge_weight_t, int> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;

typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::out_edge_iterator OutEdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, E, S, A, B;
        cin >> N >> E >> S >> A >> B;
        Graph G(N);
        WeightMap W = get(edge_weight, G);
        for (int i = 0; i < E; i++) {
            int u, v, w = INT_MAX;
            cin >> u >> v;
            for (int s = 0; s < S; s++) {
                int tmp;
                cin >> tmp;
                w = w < tmp ? w : tmp; 
            }
            Edge e; bool b;
            tie(e, b) = add_edge(v, u, G);
            W[e] = w;
        }

        for (int s = 0; s < S; s++) {
            int tmp;
            cin >> tmp;
        }

        // Find distance from node 0 to the node furthest from it.
        vector<int> D(N);

        if (S == 1) {
            // Find weight of MST
            Graph MST(N);
            vector<Edge> mst;
            kruskal_minimum_spanning_tree(G, back_inserter(mst));
            WeightMap W_MST = get(edge_weight, MST);
            for (vector<Edge>::iterator i = mst.begin(); i != mst.end(); i++) {
                Edge e; bool b;
                tie(e, b) = add_edge(source(*i, G), target(*i, G), MST); 
                W_MST[e] = W[*i];
            }
            dijkstra_shortest_paths(MST, A, distance_map(make_iterator_property_map(D.begin(), get(vertex_index, MST)))); 
        } else {
            dijkstra_shortest_paths(G, A, distance_map(make_iterator_property_map(D.begin(), get(vertex_index, G)))); 
        }

        cout << D[B] << "\n";
    }
	return 0;
}
