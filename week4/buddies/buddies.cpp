#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, no_property> Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, C, F;
        cin >> N >> C >> F;

        vector< vector<string> > A(N);
        for (int n = 0; n < N; n++) {
            A[n] = vector<string>(C);
            for (int c = 0; c < C; c++) {
                cin >> A[n][c];
            }
            sort(A[n].begin(), A[n].end());
        }

        // build Graph
        Graph G(N);
        for (int n = 0; n < N; n++) {
            for (int m = n + 1; m < N; m++) {
                vector<string> intersect;
                set_intersection(A[n].begin(), A[n].end(), A[m].begin(), A[m].end(), back_inserter(intersect));
                if (intersect.size() > F) {
                    add_edge(n, m, G);
                }
            }
        }

        // Maximum cardinality matching
        vector<Vertex> mate(N);
        edmonds_maximum_cardinality_matching(G, &mate[0]); 
        if (matching_size(G, &mate[0]) * 2 == N) {
            cout << "not ";
        }

        cout << "optimal\n";
    }
	return 0;
}
