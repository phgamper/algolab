#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/biconnected_components.hpp>
#include <boost/graph/adjacency_list.hpp>

// Namespaces
using namespace std;
using namespace boost;

struct edge_component_t {
    enum { num = 555 };
    typedef edge_property_tag kind;
} edge_component;


struct q {
    int u;
    int v;
    int k;
};

bool foo(struct q &a, struct q &b){
    return a.u < b.u || (a.u == b.u && a.v < b.v);
}



typedef adjacency_list<vecS, vecS, undirectedS, no_property, property<edge_component_t, size_t> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef property_map<Graph, edge_component_t>::type ComponentMap;


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M;
        cin >> N >> M;
        Graph G(N);
        for (int m = 0; m < M; m++) {
            int u, v;
            cin >> u >> v;
            add_edge(v, u, G);
        }
       
        ComponentMap cmap = get(edge_component, G);
        int num = biconnected_components(G, cmap);

        vector<struct q> C(num, (struct q){0});

        EdgeIt i, e;
        for (tie(i, e) = edges(G); i != e; i++){
            int u, v, k;
            u = source(*i,G);
            v = target(*i,G);
            if (u > v) {
                int tmp = u;
                u = v;
                v = tmp;
            }
            k = C[cmap[*i]].k + 1;
            C[cmap[*i]] = (struct q) { .u = u, .v = v, .k = k };
        }

        int count = 0;
        for (int j = 0; j < num; j++) {
            count = C[j].k == 1 ? count + 1 : count;
        }
        cout << count << "\n";
        sort(C, foo); 
        for (int j = 0; j < num; j++) {
            if (C[j].k == 1) {
                cout << C[j].u << " " << C[j].v << "\n";
            }
        }
    }
	return 0;
}
