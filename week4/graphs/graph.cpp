#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, property<edge_weight_t, int> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;

typedef graph_traits<Graph>::edge_iterator EdgeIt;
typedef graph_traits<Graph>::out_edge_iterator OutEdgeIt;
typedef property_map<Graph, edge_weight_t>::type WeightMap;


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M;
        cin >> N >> M;
        Graph G(N);
        WeightMap W = get(edge_weight, G);
        for (int m = 0; m < M; m++) {
            int u, v, w;
            cin >> u >> v >> w;
            Edge e; bool b;
            tie(e, b) = add_edge(v, u, G);
            W[e] = w;
        }

        // Find weight of MST
        vector<Edge> mst;
        kruskal_minimum_spanning_tree(G, back_inserter(mst));
        int weight = 0;
        for (vector<Edge>::iterator e = mst.begin(); e != mst.end(); e++) {
            weight += W[*e];
        }

        // Find distance from node 0 to the node furthest from it.
        vector<int> dmap(N);
        dijkstra_shortest_paths(G, 0, distance_map(make_iterator_property_map(dmap.begin(), get(vertex_index, G)))); 
        int dist = 0;
        for (int v = 0; v < N; v++) {
            dist = dist < dmap[v] ? dmap[v] : dist;
        }

        cout << weight << " " << dist << "\n";
    }
	return 0;
}
