#include <iostream>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

struct Bomb {
    int idx;
    int time;
    int min;

    friend bool operator< (const Bomb &i, const Bomb &j) {
        return i.min == j.min ? i.time > j.time : i.min > j.min; 
    }
};

int main(void) {
    int T, n;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n; 
        vector<int> bombs(n, 0);
        vector<int> bfs(n, 0);
        vector<bool> defused(n, false);
        for(int i = 0; i < n; i++){
            cin >> bombs[i];
        }
       
        bfs[0] = bombs[0];
        for(int i = 1; i < n; i++){
            int j = i % 2 ? (i - 1) / 2 : (i - 2) / 2;
            bfs[i] = min(bombs[i], bfs[j] - 1); 
        }

        priority_queue<Bomb> q;
        for(int i = (n-1)/2; i < n; i++){
            Bomb b = { .idx = i, .time = bombs[i], .min = bfs[i] };
            q.push(b);
        }
        
        int time = 0;
        while(!q.empty()){
            // defuse bomb
            Bomb b = q.top(); q.pop();
            // cout << "Bomb = { .idx = " << b.idx << ", .time = " << b.time << ", .min = " << b.min << " }; time = " << time << "\n"; 
            if (b.time <= time){
                time = -1;
                break;
            }
            time++;
            defused[b.idx] = true;
            // add candidates to queue
            int j = b.idx % 2 ? (b.idx - 1) / 2 : (b.idx - 2) / 2;
            if (b.idx && defused[2*j + 1] && defused[2*j + 2]){
                Bomb p = { .idx = j, .time = bombs[j], .min = bfs[j] };
                q.push(p);
            }
        }
        if(time < 0){
            cout << "no\n";
        }else{
            cout << "yes\n";
        }
    }
}
