#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > > Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;


void addEdge(int from, int to, long capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap , Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M, S;
        cin >> N >> M >> S;
        Graph G(N + 1);

        EdgeCapacityMap cmap = get(edge_capacity, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);

        for (int s = 0; s < S; s++) {
            int shop;
            cin >> shop;
            // add edge to sink 
            addEdge(shop, N, 1, cmap, rmap, G);   
        }

        for (int m = 0; m < M; m++) {
            int u, v;
            cin >> u >> v;
            addEdge(u, v, 1, cmap, rmap, G);   
            addEdge(v, u, 1, cmap, rmap, G);   
        }

        push_relabel_max_flow(G, 0, N) < S ? cout << "no\n" : cout << "yes\n";
    }
	return 0;
}
