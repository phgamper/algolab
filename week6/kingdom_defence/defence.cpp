#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > > Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;


void addEdge(int from, int to, long capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap , Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int L, P;
        cin >> L >> P;
        Graph G(L + 2);

        EdgeCapacityMap cmap = get(edge_capacity, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);

        vector<int> Given(L);
        vector<int> Needed(L);

        for (int l = 0; l < L; l++) {
            int g, d;
            cin >> g >> d;
            Given[l] = g;
            Needed[l] = d;
        }

        for (int p = 0; p < P; p++) {
            int a, b, c, C;
            cin >> a >> b >> c >> C;
            // consider moving soliders
            // add path between cities
            if (a != b) {
                Given[a] -= c;
                Given[b] += c;
                addEdge(a, b, C - c, cmap, rmap, G);   
            }
        }

        int sum = 0;
        // add path from source and to sink
        for (int l = 0; l < L; l++) {
            if (Given[l] > 0) {
                addEdge(L, l, Given[l], cmap, rmap, G);   
                addEdge(l, L + 1, Needed[l], cmap, rmap, G);   
                sum += Needed[l];
            } else {
                addEdge(l, L + 1, Needed[l] - Given[l], cmap, rmap, G);   
                sum += Needed[l] - Given[l];
            }
        }

        push_relabel_max_flow(G, L, L+1) == sum ? cout << "yes\n" : cout << "no\n";
    }
	return 0;
}
