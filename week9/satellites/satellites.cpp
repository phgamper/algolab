#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
        property<edge_residual_capacity_t, int,
            property<edge_reverse_t, Traits::edge_descriptor> > > > Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::out_edge_iterator Edges;


void addEdge(int from, int to, int capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap , Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int G, S, L;
        cin >> G >> S >> L;
        Graph Graph(G + S + 2);
        int src = G + S;
        int sink = G + S + 1;

        EdgeCapacityMap cmap = get(edge_capacity, Graph);
        ReverseEdgeMap rmap = get(edge_reverse, Graph);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, Graph);

        for (int l = 0; l < L; l++) {
            int g, s;
            cin >> g >> s;
            addEdge(g, G + s, 1, cmap, rmap, Graph);
        }

        for (int g = 0; g < G; g++) {
            addEdge(src, g, 1, cmap, rmap, Graph);
        }

        for (int s = 0; s < S; s++) {
            addEdge(G + s, sink, 1, cmap, rmap, Graph);
        }

        push_relabel_max_flow(Graph, src, sink);

        vector<bool> visited(G + S + 2, false); 
        queue<int> bfs; bfs.push(src);
        while (!bfs.empty()) {
            int item = bfs.front(); bfs.pop();
            visited[item] = true;  
            Edges e, eend;
            for (tie(e, eend) = out_edges(item, Graph); e != eend; ++e) {
                int u = target(*e, Graph);
                // cout << u << " " << rcmap[*e] << " - ";
                if (!visited[u] && rcmap[*e] > 0) {
                    bfs.push(u);
                }
            }
        }

        int gnd = 0;
        for (int g = 0; g < G; g++) {
            gnd += visited[g] ? 0 : 1;
        }
        int sat = 0;
        for (int s = 0; s < S; s++) {
            sat += visited[G + s] ? 1 : 0;
        }
        cout << gnd << " " << sat << "\n";

        for (int g = 0; g < G; g++) {
            if (!visited[g]) cout << g << " ";
        }
        for (int s = 0; s < S; s++) {
            if (visited[G + s]) cout << s << " ";
        }
        cout << "\n";
        

    }
	return 0;
}
