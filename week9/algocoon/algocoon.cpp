#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
        property<edge_residual_capacity_t, int,
            property<edge_reverse_t, Traits::edge_descriptor> > > > Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::out_edge_iterator Edges;


void addEdge(int from, int to, int capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap , Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M;
        cin >> N >> M;
        Graph Graph(N);

        EdgeCapacityMap cmap = get(edge_capacity, Graph);
        ReverseEdgeMap rmap = get(edge_reverse, Graph);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, Graph);

        for (int m = 0; m < M; m++) {
            int a, b, c;
            cin >> a >> b >> c;
            addEdge(a, b, c, cmap, rmap, Graph);
        }

        int src, sink, min = INT_MAX;

        for (int i = 1; i < N; i++) {
            // assume me taking 0
            int p = push_relabel_max_flow(Graph, 0, i);
            // assume him taking 0
            int q = push_relabel_max_flow(Graph, i, 0);
            if (p < min || q < min) {
                min = p < q ? p : q;
                src = p < q ? 0 : i;
                sink = p < q ? i : 0;
            }
        }

        int flow = push_relabel_max_flow(Graph, src, sink);

        vector<bool> visited(N, false); 
        queue<int> bfs; bfs.push(src);
        while (!bfs.empty()) {
            int item = bfs.front(); bfs.pop();
            visited[item] = true;  
            Edges e, eend;
            for (tie(e, eend) = out_edges(item, Graph); e != eend; ++e) {
                int u = target(*e, Graph);
                if (!visited[u] && rcmap[*e] > 0) {
                    bfs.push(u);
                }
            }
        }

        cout << flow << "\n";
        int count = 0;
        for (int n = 0; n < N; n++) {
            count += (int) visited[n];
        }
        cout << count;
        for (int n = 0; n < N; n++) {
            if (visited[n]) cout << " " << n;
        }
        cout << "\n";
    }
	return 0;
}
