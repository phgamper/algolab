#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

// Namespaces
using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, int,
        property <edge_weight_t, int,
            property<edge_residual_capacity_t, int,
                property<edge_reverse_t, Traits::edge_descriptor> > > > > Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type EdgeWeightMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::out_edge_iterator Edges;


void addEdge(int from, int to, int capacity, int weight, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap , Graph &G) {
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    weightmap[e] = weight;
    weightmap[reverseE] = -weight;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}


// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false); // if you use cin/cout. Do not mix cin/cout with scanf/printf calls!
	int T;
    cin >> T;
    for (int t = 0; t < T; t++) {
        int N;
        cin >> N;
        Graph G(N + 2);
        int src = 2 * N;
        int sink = 2 * N + 1;

        EdgeCapacityMap cmap = get(edge_capacity, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);
        EdgeWeightMap wmap = get(edge_weight, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);

        for (int n = 0; n < N; n++) {
            int a, c;
            cin >> a >> c;
            addEdge(src, n, a, c, cmap, wmap, rmap, G);
        }

        int students = 0;
        for (int n = 0; n < N; n++) {
            int s, p;
            cin >> s >> p;
            addEdge(n, sink, s, 20 - p, cmap, wmap, rmap, G);
            students += s;
        }

        for (int n = 0; n < N - 1; n++) {
            int v, e;
            cin >> v >> e;
            addEdge(n, n + 1, v, e, cmap, wmap, rmap, G);
        }

        successive_shortest_path_nonnegative_weights(G, src, sink);
        int cost = find_flow_cost(G);
        int flow = 0;
        Edges e, eend;
        for (tie(e, eend) = out_edges(vertex(src, G), G); e != eend; ++e) {
            flow += cmap[*e] - rcmap[*e];
        }

        flow == students ? cout << "possible " : cout << "impossible ";
        cout << flow << " " << flow * 20 - cost << "\n";
    }
	return 0;
}
