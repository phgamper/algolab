#include<iostream>
#include<vector>

using namespace std;



// bool gt (int i,int j) { return (i > j); }


struct Item {
    int cost;
    int vol;
    friend bool operator< (const Item &i, const Item &j) {
        return i.cost == j.cost ? i.vol < j.vol : i.cost < j.cost; 
    }
};

struct Punch {
    int cost;
    int dist;

    friend bool operator< (const Punch &i, const Punch &j) {
        return i.cost == j.cost ? i.dist > j.dist : i.cost < j.cost; 
    }

    friend struct Punch operator+ (const Punch &i, const Punch &j) {
        return (struct Punch) { .cost = i.cost + j.cost, .dist = i.dist + j.dist };
    }
};


int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, P; cin >> N >> P;

        vector<vector<Punch> > DP(N, vector<Punch>(P));
        // vector<int> C(N);
        //  vector<int> V(N);
        vector<Item> I(N);


        for (int n = 0; n < N; n++) {
            //cin >> C[n] >> V[n];
            int c, v;
            cin >> c >> v;
            I[n] = (struct Item) { .cost = c, .vol = v };
        }

        sort(I.begin(), I.end());

        DP[0][0] = (struct Punch) { .cost = I[0].cost, .dist = 1 };

        for (int n = 1; n < N; n++) {
            DP[n][0] = min(DP[n-1][0], (struct Punch) { .cost = I[n].cost, .dist = 1 });
        }

        for (int p = 1; p < P; p++) {
            DP[0][p] = DP[0][p-1] + (struct Punch) { .cost = I[0].cost, .dist = 0 };
            // cout << DP[0][p].cost << "/" << DP[0][p].dist << " ";
        }
        //cout << "\n";

        for (int n = 1; n < N; n++) {
            for (int p = 1; p < P; p++) {
                // pick the entry above - V[n]
                struct Punch a = p - I[n].vol < 0 ? (struct Punch) { .cost = I[n].cost, .dist = 1 } : DP[n][p - I[n].vol] + (struct Punch) { .cost = I[n].cost, .dist = 0 };
                // pick the entry just above
                // a = min(a, DP[n][p-1] + (struct Punch){ .cost = C[n], .dist = 0});
                // pick the entry to the left - V[n]
                struct Punch b = p - I[n].vol < 0 ? (struct Punch) { .cost = I[n].cost, .dist = 1 } : DP[n-1][p - I[n].vol] + (struct Punch) { .cost = I[n].cost, .dist = 1 };
                struct Punch pick = min(a, b); 
                DP[n][p] = min(DP[n-1][p], pick);
                // cout << DP[n][p].cost << "/" << DP[n][p].dist << " ";
            }
            // cout << "\n";
        }

        cout << DP[N-1][P-1].cost << " " << DP[N-1][P-1].dist << "\n";
    }
}

