#include<iostream>
#include<vector>
#include<climits>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace boost;
using namespace std;

typedef adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
    property<edge_capacity_t, long,
        property<edge_residual_capacity_t, long,
            property<edge_reverse_t, Traits::edge_descriptor,
                property <edge_weight_t, long> > > > > Graph;

typedef property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef property_map<Graph, edge_weight_t >::type EdgeWeightMap;
typedef property_map<Graph, edge_residual_capacity_t>::type ResidualCapacityMap;
typedef property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::out_edge_iterator OutEdgeIt; // Iterator

struct Ride {
    int s;
    int t;
    int d;
    int a;
    int p;
};

void addEdge(int u, int v, long c, long w, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap, Graph &G) {
    Edge e, reverseE;
    tie(e, tuples::ignore) = add_edge(u, v, G);
    tie(reverseE, tuples::ignore) = add_edge(v, u, G);
    capacitymap[e] = c;
    weightmap[e] = w;
    capacitymap[reverseE] = 0;
    weightmap[reverseE] = -w;
    revedgemap[e] = reverseE; 
    revedgemap[reverseE] = e; 
}

int main() {
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, S; cin >> N >> S;        

        vector<int> Cars(S);
        for (int s = 0; s < S; s++) {
            cin >> Cars[s];
        }

        int last = 0;
        vector<Ride> Rides(N);
        for (int n = 0; n < N; n++) {
            int s, t, d, a, p;
            cin >> s >> t >> d >> a >> p;
            Rides[n] = (struct Ride) { .s = s, .t = t, .d = d, .a = a, .p = p };
            last = a > last ? a : last;
        }

        int nodes = S * (last / 30 + 1);
        // cout << "nodes: " << nodes << "\n";
        Graph G(nodes + 2);
        int source = nodes;
        int sink = nodes + 1;
        
        EdgeCapacityMap cmap = get(edge_capacity, G);
        EdgeWeightMap wmap = get(edge_weight, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);


        for (int s = 0; s < S; s++) {
            addEdge(source, s, Cars[s], 0, cmap, wmap, rmap, G);  
            addEdge(nodes - S + s, sink, 1000, 0, cmap, wmap, rmap, G);  
        }

        for (int i = 0; i < nodes - S; i++) {
            addEdge(i, i + S, 1000, 100000, cmap, wmap, rmap, G);  
        }

        for (int n = 0; n < N; n++) {
            struct Ride r = Rides[n];
            int u = (r.d / 30) * S + r.s - 1;
            int v = (r.a / 30) * S + r.t - 1;
            int shift = (r.a - r.d) / 30;
            // cout << "u: " << u << " - v: " << v << " - shift: " << shift << " - d: " << r.d << " - a: " << r.a << "\n";
            addEdge(u, v, 1, 100000 * shift - r.p, cmap, wmap, rmap, G);  
        }
        

        successive_shortest_path_nonnegative_weights(G, source, sink);
        long cost = find_flow_cost(G);
        long flow = 0;
        // Iterate over all edges leaving the source to sum up the flow values.
        OutEdgeIt e, eend;
        for(tie(e, eend) = out_edges(vertex(source,G), G); e != eend; ++e) {
            flow += cmap[*e] - rcmap[*e];
        }
        long profit = flow * 100000 * (last / 30) - cost;
        cout << profit << "\n";

    }
}
