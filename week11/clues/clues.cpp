#include<iostream>
#include<vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/bipartite.hpp>
#include <boost/graph/connected_components.hpp>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef Triangulation::Edge_iterator Edges;
typedef Triangulation::Vertex_handle VHandle;

using namespace std;
using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property, no_property > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;


int main() {
    ios_base::sync_with_stdio (false);
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int N, M;
        K::FT P;
        cin >> N >> M >> P;
        vector<K::Point_2> J(N);
        for (int n = 0; n < N; n++) {
            cin >> J[n];
        }
        vector<K::Point_2> Clues(2 * M);
        for (int m = 0; m < M; m++) {
            cin >> Clues[m] >> Clues[2 * m + 1];
        }
        Graph G;
        int idx = 0;
        map<VHandle, int> Map;
        Triangulation dt; dt.insert(J.begin(), J.end());
        for (Edges e = dt.finite_edges_begin(); e != dt.finite_edges_end(); ++e) {
            VHandle u = e->first->vertex((e->second + 1) % 3);
            VHandle v = e->first->vertex((e->second + 2) % 3);
            K::FT d = dt.segment(e).squared_length();
            if (d <= P * P) {
                int a, b;
                if (Map.find(u) == Map.end()) {
                    a = ++idx;
                    Map.insert(pair<VHandle, int>(u, a));
                } else {
                    a = Map[u];
                }
                if (Map.find(v) == Map.end()) {
                    b = ++idx;
                    Map.insert(pair<VHandle, int>(v, b));
                } else {
                    b = Map[v];
                }
                add_edge(a, b, G);
                // cout << std::setiosflags(std::ios::fixed) << std::setprecision(0) << a << "/" << b << " | d: " << d << " - P * P: " << P * P << "\n";
            }
        }

        if (is_bipartite(G)) {

            vector<int> C(idx);
            connected_components(G, &C[0]);

            for (int m = 0; m < M; m++) {
                VHandle u = dt.nearest_vertex(Clues[m]);
                VHandle v = dt.nearest_vertex(Clues[2 * m + 1]);
                K::FT ad = squared_distance(Clues[m], u->point());
                K::FT bd = squared_distance(Clues[2 * m + 1], v->point());
                K::FT hw = squared_distance(Clues[m], Clues[2 * m + 1]);
                if (ad <= P * P && bd <= P * P) {
                    if (Map.find(u) != Map.end() && Map.find(v) != Map.end() && C[Map[u]] == C[Map[v]]) {
                        cout << "2";
                        continue;
                    } else if (hw <= P * P) {
                        cout << "1";
                        continue;
                    }
                }
                cout << "n";
            }
        } else {
            cout << "n";
        }
        cout << "\n";
    }
    return 0;
}
