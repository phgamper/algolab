#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <iostream>
#include <vector>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpzf.h>

using namespace std;

typedef CGAL::Gmpzf ET;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Quadratic_program<K::FT> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


int main()
{
    ios_base::sync_with_stdio (false);
    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int L, S, W; cin >> L >> S >> W;
        vector<K::Point_2> Lamps(L);
        for (int l = 0; l < L; l++) {
            cin >> Lamps[l];
        }
        vector<K::Point_2> Stamps(S);
        vector<int> Illum(S);
        for (int s = 0; s < S; s++) {
            cin >> Stamps[s]; 
            cin >> Illum[s];
        }
        vector<K::Segment_2> Walls(W);
        for (int w = 0; w < W; w++) {
            cin >> Walls[w];
        }

        Program lp (CGAL::SMALLER, true, 1, true, 4096);

        for (int s = 0; s < S; s++) {
            for (int l = 0; l < L; l++) {
                bool b = false;
                for (int w = 0; w < W; w++) {
                    b |= CGAL::do_intersect(Walls[w], K::Segment_2(Lamps[l], Stamps[s]));
                }
                if (!b) {
                    K::FT r = CGAL::squared_distance(Stamps[s], Lamps[l]);
                    lp.set_a(l, s, 1/r);
                    lp.set_a(l, S + s, 1/r);
                }
            }
            lp.set_b(s, Illum[s]);
            lp.set_b(S + s, 1);
            lp.set_r(S + s, CGAL::LARGER);
        }

        Solution s = CGAL::solve_linear_program(lp, ET());

        if (s.is_optimal()) {
            cout << "yes" << endl;
        } else {
            cout << "no" << endl;
        }

    };

    return 0;
}
