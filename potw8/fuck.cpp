#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <iostream>
#include <vector>
#include <tuple>
#include <cassert>
#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

using namespace std;

// choose exact floating-point type
#include <CGAL/Gmpzf.h>
typedef CGAL::Gmpzf ET; 

// program and solution types
typedef CGAL::Quadratic_program<double> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef K::Point_2 P;
typedef K::Segment_2 S;

int main() {
    ios_base::sync_with_stdio (false);
    int T;
    cin >> T;
    while(T--){
        int lamps,stamps,walls;
        cin >> lamps >> stamps >> walls;  
        
        vector<P> lampsVec;
        vector<P> stampsVec;
        vector<int> intensityVec;
        vector<S> wallsVec;
        
        for(int i = 0; i < lamps; i++){
            int x,y;
            cin >> x >> y;
            lampsVec.push_back(P(x,y));
        }
        
        for(int i = 0; i< stamps; i++){
            int x,y,I;
            cin >> x >> y >> I;
            stampsVec.push_back(P(x,y));
            intensityVec.push_back(I);
        }
        
        for(int i = 0; i < walls; i++){
            int a,b,c,d;
            cin >> a >> b >> c >> d;
            wallsVec.push_back(S(P(a,b),P(c,d)));
        }
        
        //build constraints
       Program lp (CGAL::SMALLER, true, 1, true, 4096);
        
       //decide which ones intersect
       for(int i = 0; i < stamps; i++){
           for(int j = 0; j < lamps; j++){
                S seg = S(lampsVec.at(j),stampsVec.at(i));
                bool cross = false;
                for(int k = 0; k < walls; k++){
                    S w = wallsVec.at(k);
                    cout << w << " -- " << seg << endl;
                    //intersect
                    if (CGAL::do_intersect(seg,w)) {
//                         cout << "intersect" << endl;
                        cross = true;
                        break;
                    }
                }
                
                if(!cross){
                    //do not intersect, build constraints
                    //distance
                    double d = CGAL::to_double(CGAL::squared_distance(stampsVec.at(i),lampsVec.at(j))); 
                    cout << d << " foo " << i << " - " << stamps + i << endl;
                    //intensity of lamp j for stamp i
//                     cout << "var,eq " << j << "," << 2*i << " with " << 1/d << endl;
//                     cout << "var,eq " << j << "," << 2*i+1 << " with " << 1/d << endl;
                    lp.set_a(j,i, 1/d);
                    lp.set_a(j,i + stamps, 1/d);
                }
                   
            }
            // maxIntensity for stamp i <= Mi
            lp.set_b(i,intensityVec.at(i));
            lp.set_r(i,CGAL::SMALLER);
//             cout << "eq smaller " << 2*i << " with b " << intensityVec.at(i) << endl;
//             cout << "eq larger " << 2*i+1 << " with b " << 1 << endl;
            //minIntensity for stamp i >= 1
            lp.set_b(i+stamps,1);
            lp.set_r(i+stamps,CGAL::LARGER);
        }
         
        Solution s = CGAL::solve_linear_program(lp, ET());
            
        cout << s << endl;
            
        // output
        if(s.is_optimal()){
            //CGAL::Quotient<ET> res = s.objective_value();
            //round result and output result
            std::cout << "yes" << "\n";
        }else if (s.status() == CGAL::QP_INFEASIBLE) {
            std::cout << "no" << "\n";            
        } else if(s.status() == CGAL::QP_UNBOUNDED ){
            std::cout << "no" << "\n";
        }else{ std::cout << "no" << "\n"; }
    }
  
  return 0;
}
