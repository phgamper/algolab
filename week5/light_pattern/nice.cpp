#include <iostream>
#include <vector>

using namespace std;

vector<bool> Bulbs;
vector<int> Memo;
vector<int> Flip;
int K, X;

int pattern(int i, bool bit);

int max(int a, int b) {
    return a > b ? a : b;
}

int min(int a, int b) {
    return a < b ? a : b;
}

/*
int single(int i, bool bit) {
    // TODO x = 0 is assumed
    if (Bulbs[i] == bit) {
        return pattern(i - 1, bit);
    } else {
        return 1 + pattern(i - 1, bit);
    }
}

int all(int i, bool bit) {
    if (Bulbs[i] == bit) {
        return pattern(i - 1, bit);
    } else {
        return 1 + pattern(i - 1, !bit);
    }
}


int pattern(int i, bool bit) {
    if (i == 0) {
        return Bulbs[i] == bit ? 0 : 1;
    }

    if (bit && !Flip[i]) {
        Flip[i] = min(single(i, bit), all(i, bit));
    } else if (!bit && !Memo[i]) {
        Memo[i] = min(single(i, bit), all(i, bit));
    }
    return bit ? Flip[i] : Memo[i];
}
*/


int memo(int i, bool bit) {
    if (i == 0) {
        return Bulbs[i] == bit ? 0 : 1;
    }
    if (bit && !Flip[i]) {
        if (Bulbs[i] == bit) {
            Flip[i] = memo(i - 1, bit);
        } else {
            Flip[i] = 1 + min(memo(i - 1, bit), memo(i - 1, !bit));
        }
    } else if (!bit && !Memo[i]) {
        if (Bulbs[i] == bit) {
            Memo[i] = memo(i - 1, bit);
        } else {
            Memo[i] = 1 + min(memo(i - 1, bit), memo(i - 1, !bit));
        }
    }
    return bit ? Flip[i] : Memo[i];
}

int foo(int i, bool bit) {
    if (i == 0) {
        return Bulbs[i] == bit ? 0 : 1;
    }
    if (Bulbs[i] == bit) {
        return foo(i - 1, bit);
    } else {
        return 1 + min(foo(i - 1, bit), foo(i - 1, !bit));
    }
}

/**
 * \brief check whether the light bulb i matches the pattern
 *
 * \return number of wrong bits
 */
int match(int i, int k, int x) {
    // printf("match(i = %i, k = %i, x = %08x)\n", i, k, x);
    int count = 0;
    for (int j = i; j > i - k; j--) {
        int tmp = (1 << (k - 1 - (j % k)));
        count = ((bool)(x & tmp)) == Bulbs[j] ? count : count + 1;
        // bool b = Bulbs[j];
        // printf(" > tmp = %08x, x & tmp = %08x, Bulbs[j] = %d, count = %i\n", tmp, (x & tmp), b, count);
    }
    // printf("count = %i\n", count);
    return count;
}

/**
 * \brief check window of size k, if k bits differ, flip all
 *
 */
int fook(int i, int x) {
    if (i < 0 ) {
        return 0;
    }
    int count = match(i, K, x);
    int flip = min(K - count, count) + 1;
    return min(count + fook(i - K, x),  flip + fook(i - K, ~x));
}

/**
 * \brief check window of size k, if k bits differ, flip all
 *
 * using memoization
 *
 */
int memok(int i, int x) {
    if (i < 0 ) {
        return 0;
    }
    int count = match(i, K, x);
    int flip = min(K - count, count) + 1;
    if (x == X && !Memo[i]) {
        Memo[i] = min(count + memok(i - K, x), flip + memok(i - K, ~x));
    } else if (x != X && !Flip[i]) {
        Flip[i] = min(count + memok(i - K, x), flip + memok(i - K, ~x));
    }
    return x == X ? Memo[i] : Flip[i];
}

int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        int N;
        cin >> N >> K >> X;
        Bulbs = vector<bool>(N);
        for (int n = 0; n < N; n++){
            int bit;
            cin >> bit;
            Bulbs[n] = bit & true; 
        }
        Flip = vector<int>(N, 0);
        Memo = vector<int>(N, 0);

        cout << memok(N - 1, X) << "\n";
    }
    return 0;
}
