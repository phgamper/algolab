#include <iostream>
#include <vector>

using namespace std;

vector<bool> Bulbs;
vector<int> Memo;
vector<int> Flip;
int K, X;

int min(int a, int b) {
    return a < b ? a : b;
}

/**
 * \brief check whether the light bulb i matches the pattern
 *
 * \return number of wrong bits
 */
int match(int i, int k, int x) {
    // printf("match(i = %i, k = %i, x = %08x)\n", i, k, x);
    int count = 0;
    for (int j = i; j > i - k; j--) {
        int tmp = (1 << (k - 1 - (j % k)));
        count = ((bool)(x & tmp)) == Bulbs[j] ? count : count + 1;
        // bool b = Bulbs[j];
        // printf(" > tmp = %08x, x & tmp = %08x, Bulbs[j] = %d, count = %i\n", tmp, (x & tmp), b, count);
    }
    // printf("count = %i\n", count);
    return count;
}

/**
 * \brief check window of size k, if k bits differ, flip all
 *
 */
int fook(int i, int x) {
    if (i < 0 ) {
        return 0;
    }
    int count = match(i, K, x);
    int flip = min(K - count, count) + 1;
    return min(count + fook(i - K, x),  flip + fook(i - K, ~x));
}

/**
 * \brief check window of size k, if k bits differ, flip all
 *
 * using memoization
 *
 */
int memok(int i, int x) {
    if (i < 0 ) {
        return 0;
    }
    int count = match(i, K, x);
    int flip = min(K - count, count) + 1;
    if (x == X && !Memo[i]) {
        Memo[i] = min(count + memok(i - K, x), flip + memok(i - K, ~x));
    } else if (x != X && !Flip[i]) {
        Flip[i] = min(count + memok(i - K, x), flip + memok(i - K, ~x));
    }
    return x == X ? Memo[i] : Flip[i];
}

int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        int N;
        cin >> N >> K >> X;
        Bulbs = vector<bool>(N);
        for (int n = 0; n < N; n++){
            int bit;
            cin >> bit;
            Bulbs[n] = bit & true; 
        }
        Flip = vector<int>(N, 0);
        Memo = vector<int>(N, 0);

        cout << memok(N - 1, X) << "\n";
    }
    return 0;
}
