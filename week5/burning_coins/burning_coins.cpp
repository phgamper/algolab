#include <iostream>
#include <vector>

using namespace std;


vector<int> Coins;

vector< vector<int> > Memo;


int burnMemo(int i, int j, bool mine) {
    if (mine && Memo[i][j]) {
        return Memo[i][j];
    }
    if (i == j) {
        return mine ? Coins[i] : 0;
    }
    int first = burnMemo(i + 1, j, !mine);
    int last = burnMemo(i, j - 1, !mine);
    if (mine) {
        Memo[i][j] = Coins[i] + first > Coins[j] + last ? Coins[i] + first : Coins[j] + last;
        return Memo[i][j];
    } else {
        return first < last ? first : last;
    }
}


int burn(int i, int j, bool mine) {
    if (i == j) {
        return mine ? Coins[i] : 0;
    }
    int first = burn(i + 1, j, !mine);
    int last = burn(i, j - 1, !mine);
    if (mine) {
        return Coins[i] + first > Coins[j] + last ? Coins[i] + first : Coins[j] + last;
    } else {
        return first < last ? first : last;
    }
}

int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        int N;
        cin >> N;
        Coins = vector<int>(N);
        Memo = vector< vector<int> > (N, vector<int>(N, 0));
        for (int n = 0; n < N; n++){
            cin >> Coins[n];
        }

        cout << burnMemo(0, N - 1, true) << "\n";

    }
    return 0;
}
