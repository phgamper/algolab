#include <iostream>
#include <vector>

using namespace std;


vector<int> Coins;

vector< vector<int> > Mine;
vector< vector<int> > Yours;

int max(int a, int b) {
    return a > b ? a : b;
}

int min(int a, int b) {
    return a < b ? a : b;
}

int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        int N;
        cin >> N;
        Coins = vector<int>(N);
        for (int n = 0; n < N; n++){
            cin >> Coins[n];
        }

        Mine = vector< vector<int> > (N, vector<int>(N, 0));
        Yours = vector< vector<int> > (N, vector<int>(N, 0));
        for (int n = 0; n < N; n++) {
            Mine[n][n] = Coins[n];
        }

        for (int n = 0; n < N; n++) {
            int i = 0;
            int j = n + 1;
            while (j < N) {
                Mine[i][j] = max(Coins[i] + Yours[i + 1][j], Coins[j] + Yours[i][j - 1]);
                Yours[i][j] = min(Mine[i + 1][j], Mine[i][j - 1]);
                i++;
                j++;
            }
        }

        cout << Mine[0][N - 1] << "\n";

    }
    return 0;
}
