#include <iostream>
#include <vector>

using namespace std;

int N, M;

vector<int> Brightness;
vector< vector<int> > Bulbs;


int min(int a, int b) {
    return a < b ? a : b;
}

int foo(int i, vector<int> b) {
    if(i == 0) {
        for (int m = 0; m < M; m++) {
            if (Brightness[m] != b[m]) {
                return N + 1;
            }
        }
        return 0;
    }

    vector<int> sw(M);
    for (int m = 0; m < M; m++) {
        sw[m] = b[m] + Bulbs[i-1][m];
    }

    return min(foo(i - 1, b), 1 + foo(i - 1, sw));
}


int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> N >> M;
        Bulbs = vector< vector<int> >(N, vector<int>(M));
        
        // target brightness
        Brightness = vector<int>(M);
        for (int m = 0; m < M; m++) {
            cin >> Brightness[m];  
        }

        // initial brightness
        vector<int> B (M, 0);
        for (int n = 0; n < N; n++){
            for (int m = 0; m < M; m++) {
                int on, off;
                cin >> on >> off;
                Bulbs[n][m] = (on - off) * -1;  
                B[m] += on;
            }
        }

        int res = foo(N, B);
        if (res > N ) {
            cout << "impossible\n";
        } else {
            cout << res << "\n";
        }
    }
    return 0;
}
