#include <iostream>
#include <vector>
#include <climits>

using namespace std;

int N, M;

vector< vector<int> > E;

int move(int r, int b, int turn) {
    if (r == N - 1) {
        return 0;
    }
    if (b == N - 1) {
        return 1;
    }
    
    if (turn % 2 == 1) { // Moriarty's turn
        if (turn % 4 == 3) { // move red
            int res = 0; 
            for (int i = 0; i < E[r].size(); i++) {
                res = max(move(E[r][i], b, turn + 1), res);
            }
            return res;
        } else { // move black
            int res = 0; 
            for (int i = 0; i < E[b].size(); i++) {
                res = max(move(r, E[b][i], turn + 1), res);
            }
            return res;
        }
    } else { // Holmes' turn
        if (turn % 4 == 0) { // move red
            int res = 1; 
            for (int i = 0; i < E[r].size(); i++) {
                res = min(move(E[r][i], b, turn + 1), res);
            }
            return res;
        } else { // move black
            int res = 1; 
            for (int i = 0; i < E[b].size(); i++) {
                res = min(move(r, E[b][i], turn + 1), res);
            }
            return res;
        }
    }
}

int solve(int r, int b) {
    vector< vector<int> > R = vector< vector<int> >(N, vector<int>(2));

    for (int n = 0; n < N - 1; n++) {
        R[n][0] = INT_MAX; 
        R[n][1] = INT_MIN; 
    }
    R[N-1][0] = 0; 
    R[N-1][1] = 0; 

    for (int n = N - 2; n >= 0; n--) {
        for (int e = 0; e < E[n].size(); e++) {
            int u = E[n][e];
            R[n][0] = min(R[n][0], R[u][1]);
            R[n][1] = max(R[n][1], R[u][0]);
        }
        R[n][0]++;
        R[n][1]++;
    }
    return (int) (R[r][0] > R[b][0]) || (R[r][0] == R[b][0] && R[r][0] % 2 == 0);
}



int main (void){
    int T;
    cin >> T;
    for(int t = 0; t < T; t++){
        int R, B;
        cin >> N >> M;
        cin >> R >> B;
    
        E = vector< vector<int> >(N);
        for (int m = 0; m < M; m++) {
            int u, v;
            cin >> u >> v;
            E[u-1].push_back(v-1);
        }
        cout << solve(R-1, B-1) << "\n";
//         cout << move(R-1, B-1, 0) << "\n";
    }
    return 0;
}
