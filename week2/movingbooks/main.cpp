#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool gt (int i,int j) { return (i > j); }

/**
 * Check whether r rounds are sufficient for moving the books 
 */
bool validate(int r, vector<int> *S, vector<int> *W) {
    if(S->size() * r < W->size()){
        return false;
    }
    unsigned s = 0, w = 0;
    while(s < S->size() && w < W->size()) {
        if(S->at(s) < W->at(w)){
            return false;
        }
        w += r;
        s++;
    }
    return true;
}

int main(void) {
    int T, n, m;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n >> m; 
        vector<int> S(n);
        for(int i = 0; i < n; i++){
            cin >> S[i];
        }
        sort(S.begin(), S.end(), gt);
        vector<int> W(m);
        for(int i = 0; i < m; i++){
            cin >> W[i];
        }
        sort(W.begin(), W.end(), gt);

        if(W[0] > S[0]){
            cout << "impossible\n";
            continue;
        }

        int min = 0, max = m;
        while(min < max) {
            int rounds = (min + max) / 2;
            if(validate(rounds, &S, &W)){
                max = rounds;
            } else {
                min = rounds + 1;
            }

        }
        cout << min * 3 - 1 << "\n";
    }
}
