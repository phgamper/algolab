#include <iostream>
#include <queue>
#include <climits>

using namespace std;

struct Boat {
    int pos;
    int min_end;
    int len;

    friend bool operator< (const Boat &i, const Boat &j) {
        return i.min_end == j.min_end ? i.pos > j.pos : i.min_end > j.min_end;
    }
};


int main(void) {
    int T, n;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n;
        priority_queue<Boat> q; 
        for(int i = 0; i < n; i++){
            Boat b;
            cin >> b.len >> b.pos;
            b.min_end = b.pos;
            q.push(b); 
        }

        int count = 0, l = INT_MIN;
        while(!q.empty()) {
            Boat b = q.top(); q.pop();
            if(b.min_end - b.len >= l){
                l = b.min_end;
                count++;
            } else {
                if(l <= b.pos){
                    b.min_end = l + b.len;  
                    q.push(b);
                }
            }
        }
        cout << count << "\n";
    }
}
