#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Pair {
    unsigned word;
    unsigned pos;

    friend bool operator< (const Pair &i, const Pair &j) {
        return i.pos < j.pos;
    }
};

/**
 * Check whether there is an interval of length len that meets the requirement
 */
bool validate(unsigned len, vector<Pair> *list, unsigned n) {
    vector<unsigned>  words(n,0);
    unsigned l = 0, r = 0, has = 0, w, s = list->size();
    while(r < s){
        w = list->at(r).word;
        words[w]++;
        if(words[w] == 1){
            has++;
        }
        while(list->at(r).pos - list->at(l).pos > len) {
            w = list->at(l).word;
            words[w]--;
            if(words[w] == 0){
                has--; 
            }
            l++;
        }
        if(has == n){
            return true;
        }
        r++;
    }
    return false;
}

int main(void) {
    std::ios_base::sync_with_stdio(false);
    unsigned T, n;
    cin >> T;
    for(unsigned t = 0; t < T; t++){
        cin >> n;
        vector<unsigned> M(n);
        for(unsigned i = 0; i < n; i++){
            cin >> M[i];
        }

        vector<Pair> list;
        for(unsigned i = 0; i < n; i++){
            for(unsigned j = 0; j < M[i]; j++){
                Pair f = { .word = i, .pos = 0 };
                cin >> f.pos; 
                list.push_back(f);
            }
        }
        sort(list.begin(), list.end());

        /**
         * Do a binary search
         */
        unsigned min = 0, max = list.back().pos;
        while(min < max) {
            unsigned size = (min + max) / 2;
            if(validate(size, &list, n)){
                max = size;
            } else {
                min = size + 1;
            }

        }
        cout << min + 1<< "\n";
    }
}
