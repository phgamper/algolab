#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

using namespace std;

struct Node {
    Node *parent;
    string key;
    int age;
}; 

int main(void) {
    std::ios_base::sync_with_stdio(false);
    int T, n, q;
    cin >> T;
    for(int t = 0; t < T; t++){
        cin >> n >> q; 
        unordered_map<string, Node*> M;
        string key, parent;
        int age;
        // read all species
        for(int i = 0; i < n; i++){
            cin >> key >> age;
            Node *node = new Node;
            node->parent = NULL;
            node->key = key;
            node->age = age;
            M[key] = node;
        }
        // set their parents
        for(int i = 0; i < n-1; i++){
            cin >> key >> parent;
            Node* node = M[key];
            node->parent = M[parent];
        }
        // answer queries
        for(int i = 0; i < q; i++){
            if(i > 0) {
                cout << " ";
            }
            cin >> key >> age;
            Node* item = M[key];
            Node* res = item;
            while(item != NULL && item->age <= age){
                res = item;
                item = item->parent;  
            }
            cout << res->key;
        }
        cout << "\n";
    }
}
