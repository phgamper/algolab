#include <iostream>
#include <vector>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

using namespace std;
using namespace boost;

typedef	adjacency_list_traits<vecS, vecS, directedS> Traits;
typedef adjacency_list<vecS, vecS, directedS, no_property,
	property<edge_capacity_t, long,
		property<edge_residual_capacity_t, long,
			property<edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
typedef	property_map<Graph, edge_capacity_t>::type EdgeCapacityMap;
typedef	property_map<Graph, edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	property_map<Graph, edge_reverse_t>::type ReverseEdgeMap;
typedef	graph_traits<Graph>::vertex_descriptor Vertex;
typedef	graph_traits<Graph>::edge_descriptor Edge;


void addEdge(int from, int to, long capacity, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap, Graph &G) {
//     cout << from << " -> " << to << " ** " << capacity << endl;
    Edge e, reverseE;
    bool success;
    tie(e, success) = add_edge(from, to, G);
    tie(reverseE, success) = add_edge(to, from, G);
    capacitymap[e] = capacity;
    capacitymap[reverseE] = 0;
    revedgemap[e] = reverseE;
    revedgemap[reverseE] = e;
}

int main() {

    int T; cin >> T;
    for (int t = 0; t < T; t++) {
        int M, N, K, C; cin >> M >> N >> K >> C;
        Graph G(M * N * 2 + 2);
        int source = M * N * 2;
        int sink = M * N * 2 + 1;
        EdgeCapacityMap cmap = get(edge_capacity, G);
        ReverseEdgeMap rmap = get(edge_reverse, G);
        ResidualCapacityMap rcmap = get(edge_residual_capacity, G);

        for (int k = 0; k < K; k++) {
            int x, y; cin >> x >> y; 
            addEdge(source, x + y * M, 1, cmap, rmap, G); 
        }

        // build the Graph
        for (int m = 0; m < M; m++) {
            for (int n = 0; n < N; n++) {
                // intersection capacity
                addEdge(m + n * M, N * M + m + n * M, C, cmap, rmap, G); 
                // edge to neighbours and sink
                // left
                if (m == 0) {
                    addEdge(N * M + m + n * M, sink, 1, cmap, rmap, G); 
                } else {
                    addEdge(N * M + m + n * M, (m - 1) + n * M, 1, cmap, rmap, G); 
                }
                // right
                if (m == M - 1) {
                    addEdge(N * M + m + n * M, sink, 1, cmap, rmap, G); 
                } else {
                    addEdge(N * M + m + n * M, (m + 1) + n * M, 1, cmap, rmap, G); 
                }
                // up
                if (n == 0) {
                    addEdge(N * M + m + n * M, sink, 1, cmap, rmap, G); 
                } else {
                    addEdge(N * M + m + n * M, m + (n - 1) * M, 1, cmap, rmap, G); 
                }
                // down
                if (n == N - 1) {
                    addEdge(N * M + m + n * M, sink, 1, cmap, rmap, G); 
                } else {
                    addEdge(N * M + m + n * M, m + (n + 1) * M, 1, cmap, rmap, G); 
                }
            } 
        } 
	    cout << push_relabel_max_flow(G, source, sink) << endl;
    }
    return 0;
}
